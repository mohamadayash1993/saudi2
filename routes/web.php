<?php

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localize', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
], function () {

    //Admin Panel Routes
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', function () {
            return redirect('admin/login');
        });
        Route::get('/home', 'HomeController@home')->name('home');
        Route::get('/profile', 'HomeController@profile')->name('profile');
        Route::get('/preferences', 'HomeController@preferences')->name('preferences');

        Route::post('files/single', 'FileController@single')->name('files.single');
        Route::post('files/multiple', 'FileController@multiple')->name('files.multiple');

        // Authentication Routes...
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');

        Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'Auth\RegisterController@register');


        //resources routes
        foreach (MODELS as $model) {
            Route::resource(getPlural($model), ucfirst($model) . 'Controller');
        }
    });


    //Front route
    Route::group([], function () {

        Route::get('/', 'FrontController@home')->name('front.index');
        Route::get('/home', 'FrontController@home')->name('front.home');
        Route::get('/about', 'FrontController@about')->name('front.about');
        Route::get('/faq', 'FrontController@faq')->name('front.faq');
        Route::get('/terms', 'FrontController@terms')->name('front.terms');
        Route::get('/contact', 'FrontController@contact')->name('front.contact');
        Route::get('/profile', 'FrontController@profile')->name('front.profile');
        Route::get('/wishlist', 'FrontController@wishlist')->name('front.wishlist');
        Route::get('/cart', 'FrontController@cart')->name('front.cart');

        Route::post('/cart/apply', 'FrontController@apply')->name('front.apply');

        Route::post('/checkout', 'FrontController@checkout')->name('front.checkout');
        Route::post('/execute', 'FrontController@execute')->name('front.execute');
        Route::get('/checkout', 'FrontController@checkout')->name('front.checkout');



        Route::get('/category/{id}', 'FrontController@category')->name('front.category');
        Route::get('/product/{id}', 'FrontController@product')->name('front.product');
        Route::get('/brand/{id}', 'FrontController@brand')->name('front.brand');

        Route::post('/order', 'FrontController@order')->name('front.order');
       // Route::get('/order/{id}', 'FrontController@order')->name('front.order');

        Route::post('/subscribe', 'FrontController@home')->name('front.subscribe');

        Route::post('/filter', 'FrontController@filter')->name('front.filter');
        Route::post('/review', 'FrontController@review')->name('front.review');

        Route::post('/search', 'FrontController@search')->name('front.search');
        Route::get('/search', 'FrontController@search')->name('front.search');

        Route::post('address', 'FrontController@addAddress')->name('front.address');
        Route::get('address/{id}/delete', 'FrontController@deleteAddress');
        Route::get('wishlist/{id}/delete', 'FrontController@removeProductFromWishlist');
        Route::get('wishlist/{id}/add', 'FrontController@addProductToWishlist')->name('front.addtowishlist');
        Route::get('cart/{id}/delete', 'FrontController@removeProductFromCart');
        Route::get('cart/{id}/add', 'FrontController@addProductToCart')->name('front.addToCart');
        Route::get('cart/{id}/{quantity}/update', 'FrontController@updateQuantity');

        // Authentication Routes...
        Route::get('login', 'Front\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Front\LoginController@login');
        Route::post('logout', 'Front\LoginController@logout')->name('logout');

        Route::get('register', 'Front\RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'Front\RegisterController@register');
        Route::post('update', 'Front\RegisterController@update')->name('front.update');
        Route::post('send', 'FrontController@sendConractMessage')->name('sendMsg');


    });
});

