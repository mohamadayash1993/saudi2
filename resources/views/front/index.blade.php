@extends('layouts.front')


@section('content')

    <!-- Start Main Slider-->
    <section id="main-slider">
        <div class="container p-0">
            <div id="mainSlider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($data["slides"] as $slide)
                        @if($slide->order == 1)
                            <div class="carousel-item active">
                                <img src="{{ asset('storage/'.$slide->image) }}" class="d-block w-100 animated fadeInUp"
                                     alt="">
                            </div>
                        @else
                            <div class="carousel-item">
                                <img src="{{ asset('storage/'.$slide->image) }}" class="d-block w-100 animated fadeInUp"
                                     alt="">
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="d-inline-block arrows">
                <div class="d-inline-block next"><i class="fas fa-arrow-right"></i></div>
                <div class="d-inline-block prev"><i class="fas fa-arrow-left"></i></div>
            </div>
        </div>
    </section>
    <!-- End Main Slider-->
    <!-- Start Main Categories-->
    <section id="main-categories">
        <div class="container p-0">
            <div class="gallery-row">
                @foreach($data["categories"] as $category)
                    <div class="column order-{{$category->sort}}">
                        @php($image = $category->images()->first())
                        <a href="{{ route('front.category', ['id' => $category->id]) }}" class="d-block w-100 pos-rel">
                            <img src="{{isset($image) ?  asset('storage/'.$image->path) : ''}}" class="w-100" alt=""/>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- End Main Categories-->


    @include('front.partials.featured')
    @include('front.partials.brands')

@overwrite