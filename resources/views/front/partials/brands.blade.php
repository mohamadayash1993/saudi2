<!-- Start Barnds-->
<section class="brands">
    <div class="container">
        <div class="d-block f-bold text-center main-title bordered">
            <span class="d-block h6 fcolorTxt">{{ trans('front.clients') }}</span>
            <span class="d-block h3 lcolorTxt">{{ trans('front.brands') }}</span>
        </div>
        <div class="owl-carousel part">
            @foreach($data["brands"] as $brand)
                <div class="item">
                    <a href="{{route('front.brand' , $brand->id)}}">
                        <img alt="{{getTranslatedAttribute($brand , "name")}}"
                             src="{{ asset('storage/'.$brand->image) }}"/>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!-- End  Brands-->