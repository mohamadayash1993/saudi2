<!-- Start Featured Products-->
<section class="featured-products secPad-top">
    <div class="container secPad-bottom">
        <div class="d-block f-bold text-center main-title bordered">
            <span class="d-block h6 fcolorTxt">{{trans('front.featured')}}</span>
            <span class="d-block h3 lcolorTxt">{{trans('front.products')}}</span>
        </div>
        <div class="owl-carousel products-carousel row">
            @foreach($data["featured"]  as $product)
                @php($image = $product->images->first())
                <div class="product-item">
                    <div class="d-block w-100 pos-rel text-center wrap">
                        <div class="d-block w-100 over-hidden img">
                            <img src="{{isset($image) ?  asset('storage/'.$image->path) : '' }}"
                                 alt="{{getTranslatedAttribute($product , "name")}}"
                                 class="w-100 d-block m-auto"/>
                        </div>
                        <div class="d-block w-100 over-hidden content">
                            <div class="brand f-light">{{getTranslatedAttribute($product->brand , "name")}}</div>
                            <a href="{{ route('front.product', ['id' => $product->id]) }}" class="title h6">
                                {{getTranslatedAttribute($product , "name")}}
                            </a>

                            <div class="d-block w-100 over-hidden price">
                                <span class="original h6 f-light lcolorTxt">{{$product->price}} {{trans('front.pound')}}</span>
                            </div>
                        </div>
                        <div class=" w-100 over-hidden animated fadeInDown actions">
                            <a class="btn-cart" data-id="{{$product->id}}" href="#"
                               title="{{trans('front.add to cart')}}">
                                <i class="fas fa-shopping-cart"></i>
                            </a>
                            <a class="btn-wish" data-id="{{$product->id}}"
                               title="{{trans('front.add to wish')}}">
                                <i class="far fa-heart"></i>
                            </a>
                            <a href="{{ route('front.product', ['id' => $product->id]) }}"
                               title="{{trans('front.view')}}">
                                <i class="far fa-eye"></i>
                            </a>
                        </div>
                    </div>
                </div><!--// product-item-->
            @endforeach
        </div>
        <div class="d-inline-block arrows owl">
            <div class="d-inline-block next"><i class="fas fa-arrow-right"></i></div>
            <div class="d-inline-block prev"><i class="fas fa-arrow-left"></i></div>
        </div>
    </div>
</section>
<!-- End  Featured Products-->

@push('script')
    <script>
        $(document).on('click', '.btn-wish', function () {
            let id = $(this).data('id');
            $.get('/wishlist/' + id + '/add', function (data) {
                window.location.reload();
            });
        });
    </script>

    <script>
        $(document).on('click', '.btn-cart', function () {
            let id = $(this).data('id');
            $.get('/cart/' + id + '/add', function (data) {
                window.location.reload();
            });
        });
    </script>
@endpush
