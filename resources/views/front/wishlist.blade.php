@extends('layouts.front')

@section('content')
    <!-- Start Sub Content-->
    <section class="sub-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 main-content">
                    <div class="row">
                        <div class="col-12 login">
                            <div class="d-block w-100 wcolorBG wrap">

                                <div class="d-block w-100 table-item">
                                    <div class="h5 f-bold">{{trans('front.my wish list')}}</div>
                                    @if(Auth::user()->wishlists->products()->count() > 0)
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <td class="text-left">{{trans('front.image')}}</td>
                                                <td class="text-left">{{trans('front.product')}}</td>
                                                <td class="text-left">{{trans('front.model')}}</td>
                                                <td class="text-right">{{trans('front.stock')}}</td>
                                                <td class="text-right">{{trans('front.unit price')}}</td>
                                                <td class="text-right">{{trans('front.action')}}</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach(Auth::user()->wishlists->products()->get() as $product)
                                                @php($image = $product->images()->first())
                                                <tr>
                                                    <td class="text-center"><a
                                                                href="{{route('front.product' , $product->id)}}"><img
                                                                    src="{{isset($image) ? asset('storage/'.$image->path) : ''}}"
                                                                    alt=""
                                                                    title=""></a></td>
                                                    <td class="text-left"><a
                                                                href="#">{{getTranslatedAttribute($product , "name")}}</a>
                                                    </td>
                                                    <td class="text-left">{{$product->model}}</td>
                                                    <td class="text-right">{{$product->quantity > 0 ? $product->quantity : trans('front.out')}}</td>
                                                    <td class="text-right">
                                                        <div class="price">{{$product->price}} {{{trans('front.pound')}}}
                                                        </div>
                                                    </td>
                                                    <td class="text-right">
                                                        <button data-id="{{$product->id}}"  type="button" data-toggle="tooltip"
                                                                title="{{trans('front.add to cart')}}"
                                                                class="btn btn-cart btn-primary"
                                                                data-original-title="{{trans('front.add to cart')}}">
                                                            <i class="fas fa-shopping-cart"></i>
                                                        </button>
                                                        <a href="#" data-toggle="tooltip"
                                                           data-id="{{$product->id}}"
                                                           title="{{trans('front.delete')}}"
                                                           class="btn btn-delete-product btn-danger"
                                                           data-original-title="Remove"><i class="fas fa-times"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="alert alert-info">{{trans('front.no products')}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--//main-content-->
            </div>
        </div>

    </section>
    <!-- End Sub Contact-->

@overwrite

@section('script')
    <script>
        $(document).on('click', '.btn-delete-product', function () {
            let id = $(this).data('id');
            $.get('/wishlist/' + id + '/delete', function (data) {
                window.location.reload();
            });
        });
    </script>
@overwrite