@extends('layouts.front')

@section('content')

    <!-- Start Sub Header-->
    <section class="sub-header">
        @php($image = $data["category"]->images->first())
        <div class="container  bg-fx-img"
             style="background-image: url('{{ isset($image) ?  asset("storage/".$image->path) : '' }}'); height: 200px"></div>
    </section>
    <!-- End Sub Header-->

    <!-- Start Sub Content-->
    <section class="sub-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-12 main-sidebar">
                    <div class="d-block w-100 wrap">
                        <div class="d-block side-item">
                            <div class="d-block main-title bordered lcolorTxt h5 f-bold">{{ trans('front.categories') }}</div>
                            <ul class="list-unstyled">
                                @foreach($data["categories"] as $cat)
                                    <li>
                                        <a href="{{ route('front.category', ['id' => $cat->id]) }}">{{ getTranslatedAttribute($cat, "name") }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="d-block main-title bordered lcolorTxt h5 f-bold">{{ trans('front.filters') }}</div>
                        <form id="filter-form" method="post" action="{{route('front.filter')}}">
                            @csrf
                            <input type="hidden" name="category_id" value="{{$data["category"]->id}}">
                            <div class="d-block filter-item">
                                <span class="h6 fcolorTxt f-bold title">{{trans('front.price')}}</span>
                                <input name="price" type="text" id="price-input" class="form-control">
                            </div>
                            @foreach($data["category"]->filters()->get() as $filter)
                                @if($filter->type == "multiple")
                                    <div class="d-block filter-item">
                                        <span class="h6 fcolorTxt f-bold title">{{getTranslatedAttribute($filter , "name")}}</span>
                                        @foreach(getOptions($filter->options) as $option)
                                            <div class="d-block h6 check">
                                                <input name="{{$filter->name_en}}[]" value="{{$option->code}}"
                                                       type="checkbox"
                                                       id="option-{{$option->id}}" class="aa-check">
                                                <label for="option-{{$option->id}}">{{getTranslatedAttribute($option , "text")}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                @elseif($filter->type == "options")
                                    <div class="d-block filter-item">
                                        <span class="h6 fcolorTxt f-bold title">{{getTranslatedAttribute($filter , "name")}}</span>
                                        <div class="d-block aa-select">
                                            <select name="{{$filter->name_en}}" id="" class="form-control">
                                                @foreach(getOptions($filter->options) as $option)
                                                    <option value="{{$option->code}}">{{getTranslatedAttribute($option , "text")}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            <button type="submit" form="filter-form"
                                    class="butn butnLight wcolorTxt h6">{{trans('front.filter')}}</button>

                        </form>
                    </div>
                </div><!--//main-sidebar-->
                <div class="col-lg-9 col-md-8 col-12 main-content">
                    <div class="d-block description">
                        <div class="d-block main-title bordered lcolorTxt h5 f-bold">
                            {{ getTranslatedAttribute($data["category"], "name") }}
                        </div>
                        <div class="d-block jLeft fcolorTxt h6">
                            {!! getTranslatedAttribute($data["category"], "body") !!}
                        </div>
                    </div>
                    @if($data["category"]->children()->count() > 0)

                        <div class="gallery-row sub-cats">
                            @foreach($data["category"]->children()->get() as $cat)
                                @php($image = $cat->images->first())
                                <div class="column order-{{ $cat->sort }}">
                                    <a href="{{ route('front.category', ['id' => $cat->id]) }}"
                                       class="d-block w-100 pos-rel">
                                        <img src="{{isset($image) ?  asset('storage/'.$image->path) : '' }}"
                                             class="w-100"
                                             alt="{{getTranslatedAttribute($cat , "name")}}"/>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    @if($data["category"]->products()->count() > 0)
                        <div class="row p-0 products">
                            <div class="col-12 main-title bordered lcolorTxt h5 f-bold">{{ trans('front.categoryProducts') }}</div>

                            <div class="col-12 row p-0 grcolorBG setting">
                                <ul class="col-md-6 col-12 m-0 list-unstyled  ">
                                    <li class="d-inline-block">
                                        <a href="#" class="fcolorTxt h4 grid-btn">
                                            <i class="fas fa-th"></i>
                                        </a>
                                    </li>
                                    <li class="d-inline-block">
                                        <a href="#" class="fcolorTxt h4 list-btn">
                                            <i class="fas fa-list"></i>
                                        </a>
                                </ul>
                            </div>
                            @foreach($data["category"]->products()->get() as $product)
                                @php($image = $product->images->first())
                                <div class="col-xl-3 col-lg-4 col-md-6 col-12 product-item">
                                    <div class="d-block w-100 pos-rel text-center wrap">
                                        <div class="d-block w-100 over-hidden img">
                                            <img src="{{isset($image) ?  asset('storage/'.$image->path) : '' }}"
                                                 alt="{{getTranslatedAttribute($product , "name")}}"
                                                 class="w-100 d-block m-auto"/>
                                        </div>

                                        <div class="d-block w-100 over-hidden content">
                                            <div class="brand f-light">{{ getTranslatedAttribute($product->brand, "name") }}</div>
                                            <a href="{{ route('front.product', ['id' => $product->id]) }}"
                                               class="title h6">
                                                {{ getTranslatedAttribute($product, "name") }}
                                            </a>

                                            <div class="d-block w-100 over-hidden price">
                                                <span class="original h6 f-light lcolorTxt">{{$product->price}} {{trans('front.pound')}}</span>
                                            </div>
                                        </div>

                                        <div class=" w-100 over-hidden animated fadeInDown actions">
                                            <a class="btn-cart" data-id="{{$product->id}}" href="#" title="{{trans('front.add to cart')}}">
                                                <i class="fas fa-shopping-cart"></i>
                                            </a>
                                            <a class="btn-wish" data-id="{{$product->id}}" href="#" title="{{trans('front.add to wish')}}">
                                                <i class="far fa-heart"></i>
                                            </a>
                                            <a href="{{ route('front.product', ['id' => $product->id]) }}"
                                               title="{{trans('front.view')}}">
                                                <i class="far fa-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--// product-item-->
                            @endforeach

                        </div>
                    @endif
                </div><!--//main-content-->
            </div>
        </div>

    </section>
    <!-- End Sub Contact-->


    @include('front.partials.featured')
    @include('front.partials.brands')

@overwrite
