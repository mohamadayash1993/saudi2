@extends('layouts.front')

@section('content')
    <!-- Start Sub Content-->
    <section class="sub-content">
        <div class="container">
            <div class="row">
                <div class="col-12 main-content">
                    <form id="checkout-form" method="post" action="{{route('front.execute')}}">
                        @csrf
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading grcolorBG">

                                    <a href="#collapse-payment-address" data-toggle="collapse"
                                       data-parent="#accordion" class="accordion-toggle  aa-link fcolorTxt h4"
                                       aria-expanded="true">{{trans('front.billing address')}}
                                        <i class="fas fa-caret-down"></i>
                                    </a>
                                </div>
                                <div class="panel-collapse collapse in" id="collapse-payment-address"
                                     aria-expanded="true"
                                     style="">
                                    <div class="panel-body">
                                        <label for="billing-address">{{trans('front.select from existing')}}</label>
                                        @if(Auth::user()->addresses()->count())
                                            <select required id="billing-address" name="billing_address"
                                                    class="form-control">
                                                <option></option>
                                                @foreach(Auth::user()->addresses()->get() as $address)
                                                    <option value="{{$address->id}}">{{$address->name}}
                                                        - {{$address->details}}</option>
                                                @endforeach
                                            </select>

                                        @endif
                                        <br>
                                        <h6>{{trans('front.create new')}}</h6>
                                        <form method="post" action="{{route('front.address')}}">
                                            @csrf
                                            <input type="hidden" name="checkout" value="1">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="address-name">{{trans('front.name')}}</label>
                                                        <input type="text" name="name" id="address-name"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="address-details">{{trans('front.details')}}</label>
                                                        <input type="text" name="details" id="address-details"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-success">{{trans('front.create')}}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading grcolorBG">

                                    <a href="#collapse-delivery-address" data-toggle="collapse"
                                       data-parent="#accordion" class="accordion-toggle  aa-link fcolorTxt h4"
                                       aria-expanded="true">{{trans('front.delivery address')}}
                                        <i class="fas fa-caret-down"></i>
                                    </a>
                                </div>
                                <div class="panel-collapse collapse in" id="collapse-delivery-address"
                                     aria-expanded="true"
                                     style="">
                                    <div class="panel-body">
                                        <label for="delivery-address">{{trans('front.select from existing')}}</label>
                                        @if(Auth::user()->addresses()->count())
                                            <select required id="delivery-address" name="delivery_address"
                                                    class="form-control">
                                                <option></option>
                                                @foreach(Auth::user()->addresses()->get() as $address)
                                                    <option value="{{$address->id}}">{{$address->name}}
                                                        - {{$address->details}}</option>
                                                @endforeach
                                            </select>

                                        @endif
                                        <br>
                                        <h6>{{trans('front.create new')}}</h6>
                                        <form method="post" action="{{route('front.address')}}">
                                            @csrf
                                            <input type="hidden" name="checkout" value="1">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="address-name">{{trans('front.name')}}</label>
                                                        <input type="text" name="name" id="address-name"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="address-details">{{trans('front.details')}}</label>
                                                        <input type="text" name="details" id="address-details"
                                                               class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-success">{{trans('front.create')}}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="text-center">
                            <button form="checkout-form" type="submit"
                                    class="btn btn-primary">{{trans('front.confirm order')}}</button>
                        </div>
                    </form>
                </div><!--//main-content-->
            </div>
        </div>

    </section>
    <!-- End Sub Contact-->
    <br>
@overwrite