@extends('layouts.front')

@section('content')
<!-- Start Sub Content-->
<section class="sub-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-12 main-sidebar">
                <div class="d-block w-100 wrap">
                    <div class="d-block side-item links">
                        <div class="d-block main-title bordered lcolorTxt h5 f-bold">My Account</div>
                        <ul class="list-unstyled">
                            <li><a href="#" class="active">Account Information</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                        </ul>
                    </div>
                </div>

            </div><!--//main-sidebar-->
            <div class="col-md-9 col-12 main-content">
                <div class="row">
                    <div class="col-12 login">
                        <div class="d-block w-100 wcolorBG wrap">

                            <div class="d-block w-100">
                                <div class="h5 f-bold">Account Logout</div>
                                <div class="h6 f-light">
                                    You have been logged off your account. It is now safe to leave the computer.
                                    Your shopping cart has been saved, the items inside it will be restored whenever you log back into your account.
                                </div>
                                <a href="#" class="d-inline-block  butn butnLight wcolorTxt h6">Continue</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--//main-content-->
        </div>
    </div>

</section>
<!-- End Sub Contact-->

@overwrite