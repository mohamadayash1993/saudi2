@extends('layouts.front')

@section('content')
<!-- Start Sub Content-->
<section class="sub-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-12 main-sidebar">
                <div class="d-block w-100 wrap">
                    <div class="d-block side-item links">
                        <div class="d-block main-title bordered lcolorTxt h5 f-bold">My Account</div>
                        <ul class="list-unstyled">
                            <li><a href="#" class="active">Account Information</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                        </ul>
                    </div>
                </div>

            </div><!--//main-sidebar-->
            <div class="col-md-9 col-12 main-content">
                <div class="row">
                    <div class="col-12 login">
                        <div class="d-block w-100 wcolorBG wrap">
                            <div class="d-block w-100 table-item">
                                <div class="h5 f-bold">Address Book Entries</div>
                                <table class="table table-bordered table-hover h6">
                                    <tbody>
                                    <tr>
                                        <td class="text-left">
                                            <p>lorem Ipusm</p>
                                            <p>lorem Ipusm</p>
                                            <p>lorem Ipusm</p>
                                            <p>lorem Ipusm</p>
                                            <p>lorem Ipusm</p>
                                            <p>lorem Ipusm</p>
                                            <p>lorem Ipusm</p>
                                            <p>lorem Ipusm</p>
                                        </td>
                                        <td class="text-right">
                                            <a href="#" class="btn btn-info">Edit</a> &nbsp;
                                            <a href="#" class="btn btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" colspan="5">You do not have any Addresss!</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--//main-content-->
        </div>
    </div>

</section>
<!-- End Sub Contact-->

@overwrite