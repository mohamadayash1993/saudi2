@extends('layouts.front')

@section('content')
<!-- Start Sub Content-->
<section class="sub-content">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-12 main-content">
                <div class="row">
                    <div class="col-12 h5 f-bold main-title bordered lcolorTxt">{{ trans('front.contact') }}</div>
                    <div class="col-12 h6">
                        <div class="d-block main-title h1 wcolorTxt">
                            <div class="f-light animated fadeInUp h3">{{ trans('front.question') }}</div>
                            <div class="f-bold animated fadeInDown">{{ trans('front.message') }}</div>
                        </div>
                        <form action="{{ route('sendMsg') }}" method="post" class="row p-0">
                            @csrf
                            <div class="col-lg-6 col-12 form-group">
                                <input type="text" placeholder="{{ trans('front.name') }}" name="name" class="form-control white"/>
                            </div>
                            <div class="col-lg-6 col-12 form-group">
                                <input type="email" placeholder="{{ trans('front.email') }}" name="email" class="form-control white"/>
                            </div>
                            <div class="col-12 form-group">
                                <input type="text" placeholder="{{ trans('front.subject') }}" name="subject" class="form-control  white"/>
                            </div>
                            <div class="col-12 form-group">
                        <textarea name="msg" id="" cols="30" rows="5" placeholder="{{ trans('front.msg') }}"
                                  class="form-control white"></textarea>
                            </div>
                            <div class="col-12 form-group">
                                <button type="submit" class="butn butnWhite h6 dcolorTxt">
                                    <span class="mr-1"><i class="fas fa-envelope-open"></i></span>
                                    <span>{{ trans('front.send') }}</span>
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div><!--//main-content-->
        </div>
    </div>

</section>
<!-- End Sub Contact-->
@overwrite