@extends('layouts.front')

@section('content')
<!-- Start Sub Content-->
<section class="sub-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-12 main-sidebar">
                <div class="d-block w-100 wrap">
                    <div class="d-block side-item links">
                        <div class="d-block main-title bordered lcolorTxt h5 f-bold">Quick links</div>
                        <ul class="list-unstyled">
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                        </ul>
                    </div>
                </div>

            </div><!--//main-sidebar-->
            <div class="col-lg-9 col-md-8 col-12 main-content">
                        <div class="row">
                            <div class="col-md-6 col-12 login">
                                <div class="d-block w-100 wcolorBG wrap">
                                    <form action="" class="d-block w-100 p-0">
                                        <div class="h5 f-bold">Returning Customer</div>
                                        <div class="h6 f-light">I am a returning customer</div>
                                        <div class="form-group required">
                                            <div class="d-block">
                                                <label class="control-label" for="input-name">Your Email</label>
                                                <input type="text" name="name" value="" id="input-name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group required">
                                            <div class="d-block">
                                                <label class="control-label" for="input-name2">Your password</label>
                                                <input type="password" name="name2" value="" id="input-name2" class="form-control">
                                            </div>
                                        </div>
                                        <a href="#">Forgotten Password</a>
                                        <div class="form-group required">
                                            <div class="d-block">
                                                <button type="submit" class="butn butnLight wcolorTxt h6">Login</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6 col-12 login register">

                                <div class="d-block w-100 wcolorBG wrap">
                                        <div class="h5 f-bold">New Customer</div>
                                        <div class="h6 f-light jLeft">
                                            By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.
                                        </div>
                                    <a href="#" class="butn butnLight wcolorTxt h6">Register</a>
                                </div>
                            </div>
                        </div>
            </div><!--//main-content-->
        </div>
    </div>

</section>
<!-- End Sub Contact-->
@overwrite