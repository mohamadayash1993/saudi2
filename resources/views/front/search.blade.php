@extends('layouts.front')

@section('content')
    <!-- Start Sub Content-->
    <section class="sub-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-12 main-sidebar">
                    <div class="d-block w-100 wrap">
                        <div class="d-block side-item">
                            <div class="d-block main-title bordered lcolorTxt h5 f-bold">{{ trans('front.categories') }}</div>
                            <ul class="list-unstyled">
                                @foreach($data["categories"] as $cat)
                                    <li>
                                        <a href="{{ route('front.category', ['id' => $cat->id]) }}">{{ getTranslatedAttribute($cat, "name") }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="d-block main-title bordered lcolorTxt h5 f-bold">{{ trans('front.filters') }}</div>
                        <form id="filter-form" method="post" action="{{route('front.filter')}}">
                            @csrf
                            <div class="d-block filter-item">
                                <span class="h6 fcolorTxt f-bold title">{{trans('front.price')}}</span>
                                <input name="price" type="text" id="price-input" class="form-control">
                            </div>
                            <button type="submit" form="filter-form"
                                    class="butn butnLight wcolorTxt h6">{{trans('front.filter')}}</button>

                        </form>
                    </div>
                </div><!--//main-sidebar-->

                <div class="col-lg-9 col-md-8 col-12 main-content">
                    <div class="d-block description">
                        <div class="d-block main-title bordered lcolorTxt h5 f-bold">{{trans('front.search for')}}
                            "{{$data["query"]}}" :
                        </div>
                        <div class="d-block fcolorTxt h6 f-light">{{$data["products"]->total()}} {{trans('front.results found')}} </div>
                    </div>

                    <div class="row p-0 products">
                        <div class="col-12 row p-0 grcolorBG setting">
                            <ul class="col-md-6 col-12 m-0 list-unstyled ">
                                <li class="d-inline-block">
                                    <a href="#" class="fcolorTxt h4 grid-btn">
                                        <i class="fas fa-th"></i>
                                    </a>
                                </li>
                                <li class="d-inline-block">
                                    <a href="#" class="fcolorTxt h4 list-btn">
                                        <i class="fas fa-list"></i>
                                    </a>
                            </ul>
                        </div>

                        @foreach($data["products"] as $product)
                            @php($image = $product->images->first())
                            <div class="col-xl-3 col-lg-4 col-md-6 col-12 product-item">
                                <div class="d-block w-100 pos-rel text-center wrap">
                                    <div class="d-block w-100 over-hidden img">
                                        <img src="{{isset($image) ?  asset('storage/'.$image->path) : '' }}"
                                             alt="{{getTranslatedAttribute($product , "name")}}"
                                             class="w-100 d-block m-auto"/>
                                    </div>

                                    <div class="d-block w-100 over-hidden content">
                                        <div class="brand f-light">{{ getTranslatedAttribute($product->brand, "name") }}</div>
                                        <a href="{{ route('front.product', ['id' => $product->id]) }}"
                                           class="title h6">
                                            {{ getTranslatedAttribute($product, "name") }}
                                        </a>

                                        <div class="d-block w-100 over-hidden price">
                                            <span class="original h6 f-light lcolorTxt">{{$product->price}} {{trans('front.pound')}}</span>
                                        </div>
                                    </div>

                                    <div class=" w-100 over-hidden animated fadeInDown actions">
                                        <a class="btn-cart" data-id="{{$product->id}}" href="#" title="{{trans('front.add to cart')}}">
                                            <i class="fas fa-shopping-cart"></i>
                                        </a>
                                        <a class="btn-wish" data-id="{{$product->id}}" href="#" title="{{trans('front.add to wish')}}">
                                            <i class="far fa-heart"></i>
                                        </a>
                                        <a href="{{ route('front.product', ['id' => $product->id]) }}"
                                           title="{{trans('front.view')}}">
                                            <i class="far fa-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div><!--// product-item-->
                        @endforeach


                    </div>
                    <div class="col-12 row p-0 grcolorBG setting pages">
                        @php($num = $data["products"]->total() > 10 ? 10 : $data["products"]->total())
                        <div class="col-md-6 col-12 fcolorTxt  ">{{trans('front.showing' , ['number' => $num,'total' => $data["products"]->total()])}}</div>
                        <ul class="col-md-6 col-12 m-0 list-unstyled">
                            {{$data["products"]->appends(["query" => $data["query"]])->links()}}
                        </ul>

                    </div>
                </div><!--//main-content-->
            </div>
        </div>

    </section>
    <!-- End Sub Contact-->

    @include('front.partials.featured')
    @include('front.partials.brands')

@overwrite