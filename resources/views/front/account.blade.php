@extends('layouts.front')

@section('content')
    <!-- Start Sub Content-->
    <section class="sub-content">
        <div class="container">
            @if(session()->has('message'))
                <div class="alert alert-success">{{session('message')}}</div>
            @endif
            <div class="row">
                <div class="col-md-12 col-12 main-content">
                    <div class="row">
                        <div class="col-12 login">
                            <div class="d-block w-100 wcolorBG wrap">
                                <form method="post" action="{{route('front.update')}}" class="d-block w-100 p-0">
                                    @csrf
                                    <div class="h5 f-bold">{{trans('front.information')}}</div>
                                    <div class="h6 f-light">{{trans('front.account details')}}</div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label" for="name">{{trans('front.name')}}</label>
                                            <input type="text" name="name" value="{{Auth::user()->name}}" id="name"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label" for="email">{{trans('front.email')}}</label>
                                            <input type="email" name="email" value="{{Auth::user()->email}}" id="email"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label" for="mobile">{{trans('front.mobile')}}</label>
                                            <input type="tel" name="mobile" value="{{Auth::user()->mobile}}" id="mobile"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <button type="submit"
                                                    class="butn butnLight wcolorTxt h6">{{trans('front.update')}}</button>
                                        </div>
                                    </div>
                                </form>


                                <form method="post" action="{{route('front.update')}}" class="d-block w-100 p-0">
                                    @csrf
                                    <div class="h5 f-bold">{{trans('front.change password')}}</div>
                                    <div class="h6 f-light">{{trans('front.your password')}}</div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label"
                                                   for="password">{{trans('front.password')}}</label>
                                            <input type="password" name="password" value=""
                                                   id="password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label"
                                                   for="password_confirmation">{{trans('front.password confirmation')}}</label>
                                            <input type="password" name="password_confirmation"
                                                   value="" id="password_confirmation"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <button type="submit"
                                                    class="butn butnLight wcolorTxt h6">{{trans('front.update')}}</button>
                                        </div>
                                    </div>
                                </form>

                                <div class="d-block w-100 table-item">
                                    <div class="h5 f-bold">{{trans('front.address book')}}</div>
                                    @if(Auth::user()->addresses()->count())
                                        <table class="table table-bordered table-hover h6">
                                            <tbody>
                                            @foreach(Auth::user()->addresses()->get() as $address)
                                                <tr>
                                                    <td class="text-left">
                                                        {{$address->name}}
                                                    </td>
                                                    <td class="text-left">
                                                        {{$address->details}}
                                                    </td>
                                                    <td class="text-right">&nbsp;
                                                        <a href="#" data-id="{{$address->id}}"
                                                           class="btn btn-delete-address btn-danger">{{trans('front.delete')}}</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="alert alert-info">{{trans('front.no addresses')}}</div>
                                    @endif

                                    <form method="post" action="{{route('front.address')}}">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="address-name">{{trans('front.name')}}</label>
                                                    <input type="text" name="name" id="address-name" required
                                                           class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="address-details">{{trans('front.details')}}</label>
                                                    <input type="text" name="details" id="address-details" required
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-success">{{trans('front.create')}}</button>
                                    </form>
                                </div>


                                <div class="d-block w-100 table-item">
                                    <div class="h5 f-bold">{{trans('front.my wishlist')}}</div>
                                    @if(Auth::user()->wishlists->products()->count() > 0)
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <td class="text-left">{{trans('front.product')}}</td>
                                                <td class="text-left">{{trans('front.model')}}</td>
                                                <td class="text-right">{{trans('front.stock')}}</td>
                                                <td class="text-right">{{trans('front.unit price')}}</td>
                                                <td class="text-right">{{trans('front.action')}}</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach(Auth::user()->wishlists->products()->get() as $product)
                                                <tr>
                                                    <td class="text-left"><a
                                                                href="#">{{getTranslatedAttribute($product , "name")}}</a>
                                                    </td>
                                                    <td class="text-left">{{$product->model}}</td>
                                                    <td class="text-right">{{$product->quantity > 0 ? $product->quantity : trans('front.out')}}</td>
                                                    <td class="text-right">
                                                        <div class="price">{{$product->price}} {{{trans('front.pound')}}}
                                                        </div>
                                                    </td>
                                                    <td class="text-right">
                                                        <button data-id="{{$product->id}}" type="button"
                                                                data-toggle="tooltip"
                                                                title="{{trans('front.add to cart')}}"
                                                                class="btn btn-cart btn-primary"
                                                                data-original-title="{{trans('front.add to cart')}}">
                                                            <i class="fas fa-shopping-cart"></i>
                                                        </button>
                                                        <a href="#" data-toggle="tooltip"
                                                           data-id="{{$product->id}}"
                                                           title="{{trans('front.delete')}}"
                                                           class="btn btn-delete-product btn-danger"
                                                           data-original-title="Remove"><i class="fas fa-times"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="alert alert-info">{{trans('front.no products')}}</div>
                                    @endif
                                </div>


                                <div class="d-block w-100 table-item">
                                    <div class="h5 f-bold">{{trans('front.your orders')}}</div>
                                    <div class="table-responsive">
                                        @if(Auth::user()->orders()->count() > 0)
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <td class="text-left">{{trans('front.date added')}}</td>
                                                    <td class="text-left">{{trans('front.total quantity')}}</td>
                                                    <td class="text-right">{{trans('front.total price')}}</td>
                                                    <td class="text-right">{{trans('front.status')}}</td>
                                                    <td class="text-right">{{trans('front.action')}}</td>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach(Auth::user()->orders()->get() as $order)
                                                    <tr>
                                                        <td class="text-left">{{$order->created_at}}</td>
                                                        <td class="text-left">{{$order->products()->sum('orders_products.quantity')}}</td>
                                                        <td class="text-left">{{$order->total_price}} {{trans('front.pound')}}</td>
                                                        <td class="text-left">{{getOption("orders_status" , $order->status)}}</td>
                                                        <td class="text-right">
                                                            <a href="{{route('front.order' , $order->id)}}"
                                                               type="button" data-toggle="tooltip"
                                                               title="{{trans('front.view details')}}"
                                                               class="btn btn-view btn-primary">
                                                                <i class="fas fa-eye"></i>
                                                            </a>
                                                            <a href="#" data-toggle="tooltip"
                                                               data-id="{{$order->id}}"
                                                               title="{{trans('front.cancel')}}"
                                                               class="btn btn-cancel-order btn-danger"><i
                                                                        class="fas fa-times"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-info">{{trans('front.no orders')}}</div>
                                        @endif
                                    </div>
                                </div>

                                <form class="p-0" method="post" action="{{route('front.subscribe')}}">
                                    <div class="h5 f-bold">{{trans('front.newsletter subscription')}}</div>
                                    <div class="h6 f-light">{{trans('front.subscribe')}}</div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <input type="radio" name="subscribe[]" value="1" class="aa-radio">
                                            &nbsp;{{trans('front.yes')}}
                                            &nbsp;
                                            <input type="radio" name="subscribe[]" value="0" class="aa-radio">
                                            &nbsp;{{trans('front.no')}}
                                        </div>
                                    </div>
                                    <div class="buttons clearfix">
                                        <div class="pull-right">
                                            <button type="button" id="" data-loading-text="Loading..."
                                                    class="butn butnLight wcolorTxt h6">{{trans('front.update')}}
                                            </button>
                                        </div>
                                    </div>
                                </form>

                                <div class="d-block w-100">
                                    <div class="h5 f-bold">{{trans('front.account logout')}}</div>
                                    <div class="h6 f-light">
                                        {{trans('front.logout message')}}
                                    </div>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();" href="#"
                                       class="d-inline-block  butn butnLight wcolorTxt h6">{{trans('front.logout')}}</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--//main-content-->
            </div>
        </div>

    </section>
    <!-- End Sub Contact-->

@overwrite
@section('script')
    <script>
        $(document).on('click', '.btn-delete-address', function () {
            let id = $(this).data('id');
            $.get('/address/' + id + '/delete', function (data) {
                window.location.reload();
            });
        });

        $(document).on('click', '.btn-delete-product', function () {
            let id = $(this).data('id');
            $.get('/wishlist/' + id + '/delete', function (data) {
                window.location.reload();
            });
        });
    </script>
@overwrite