@extends('layouts.front')

@section('content')
    <!-- Start Sub Content-->
    <section class="sub-content">
        <div class="container">
            <div class="row">
                @if(session()->has('order_message'))
                    <div class="alert alert-danger">{{session('message')}}</div>
                    @php(session()->forget('order_message'))
                @endif
                <div class="col-12 main-content">
                    <div class="alert alert-danger alert-dismissible"><i class="fas fa-exclamation-circle"></i>
                        {{trans('front.stock note')}}
                        <button type="button" class="close" data-dismiss="alert">×</button>
                    </div>
                    <div class="h5 f-bold">{{trans('front.your cart')}}</div>
                    <div class="h6 f-light">{{trans('front.you have' , ['num' => session('cart')->getTotalQuantity()])}}</div>
                    <form id="order-form" action="{{route('front.order')}}" method="post"
                          enctype="multipart/form-data" class="p-0">
                        @csrf
                        <input type="hidden" name="delivery_address" value="{{$data["delivery_address"]->id}}">
                        <input type="hidden" name="billing_address" value="{{$data["billing_address"]->id}}">
                        <div class="table-responsive">
                            @if(session('cart')->getTotalQuantity() > 0)
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <td class="text-left">{{trans('front.image')}}</td>
                                        <td class="text-left">{{trans('front.product')}}</td>
                                        <td class="text-left">{{trans('front.model')}}</td>
                                        <td class="text-right">{{trans('front.quantity')}}</td>
                                        <td class="text-right">{{trans('front.unit price')}}</td>
                                        <td class="text-right">{{trans('front.total')}}</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach(session('cart')->getProducts() as $product)
                                        @php($image = $product->images()->first())
                                        <tr>
                                            <td class="text-center"><a
                                                        href="{{route('front.product' , $product->id)}}"><img
                                                            src="{{isset($image) ? asset('storage/'.$image->path) : ''}}"
                                                            alt=""
                                                            title=""></a></td>
                                            <td class="text-left">
                                                <a
                                                        href="#">{{getTranslatedAttribute($product , "name")}}
                                                    @if($product->cart_quantity > $product->quantity)
                                                        <span class="help-block text-danger">***</span>
                                                    @endif
                                                </a>
                                            </td>
                                            <td class="text-left">{{$product->model}}</td>
                                            <td class="text-left">
                                                <div class="input-group btn-block" style="max-width: 200px;">
                                                    <input data-id="{{$product->id}}" data-price="{{$product->price}}"
                                                           type="text"
                                                           name="quantity[]" value="{{$product->cart_quantity}}"
                                                           size="1"
                                                           class="form-control quantity-input">
                                                    <span class="input-group-btn">
                                        <a data-toggle="tooltip" title="" class="btn btn-refresh btn-primary"
                                           data-original-title="{{trans('front.update')}}"><i
                                                    class="fas fa-sync"></i></a>
                                        <a data-id="{{$product->id}}" data-toggle="tooltip" title=""
                                           class="btn btn-remove-cart btn-danger"
                                           data-original-title="{{trans('front.delete')}}"><i
                                                    class="fas fa-times"></i></a>
                                        </span>
                                                </div>
                                            </td>
                                            <td class="text-right">{{$product->price}} {{trans('front.pound')}}</td>
                                            <td class="text-right td-price">{{$product->price}} {{trans('front.pound')}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="alert alert-info">{{trans('front.no products')}}</div>
                            @endif
                        </div>
                    </form>

                    @if(session()->has("message"))
                        <div class="alert alert-info">{{session()->get("message")}}</div>
                        @php(session()->forget('message'))
                    @endif
                    @php($coupon = session('cart')->getCoupon())
                    @if(isset($coupon))
                        <div class="alert alert-info">{{trans('front.discount is' , ['num' => $coupon->percentage])}}</div>
                    @endif
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading grcolorBG">
                                <a href="#collapse-coupon" class="accordion-toggle aa-link fcolorTxt h4"
                                   data-toggle="collapse" data-parent="#accordion"
                                   aria-expanded="true">
                                    {{trans('front.use coupon')}}
                                    <i class="fas fa-caret-down"></i>
                                </a>
                            </div>
                            <div id="collapse-coupon" class="panel-collapse collapse in" aria-expanded="true" style="">
                                <div class="panel-body">
                                    <form id="coupon-form" method="post" action="{{route('front.apply')}}">
                                        @csrf
                                        <label class="d-block fcolorTxt h6"
                                               for="input-coupon">{{trans('front.enter coupon')}}</label>

                                        <input required type="text" name="coupon"
                                               value="{{isset($coupon) ? $coupon->code : ''}}"
                                               placeholder="{{trans('front.enter coupon')}}"
                                               id="input-coupon" class="form-control d-block">

                                        <input type="submit" id="button-coupon"
                                               data-loading-text="Loading..."
                                               value="{{trans('front.apply coupon')}}"
                                               class="butn butnLight wcolorTxt h6 btn-apply"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td>{{trans('front.billing address')}}</td>
                                    <td>{{$data["billing_address"]->name}}</td>
                                    <td>{{$data["billing_address"]->details}}</td>
                                </tr>
                                <tr>
                                    <td>{{trans('front.delivery address')}}</td>
                                    <td>{{$data["delivery_address"]->name}}</td>
                                    <td>{{$data["delivery_address"]->details}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td class="text-right"><strong>{{trans('front.total')}}</strong></td>
                                    <td class="text-right" id="cart-total">{{session('cart')->getTotal()}}</td>
                                    <td class="text-right" id="cart-total">{{trans('front.pound')}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>{{trans('front.tax')}} (5%)</strong></td>
                                    <td class="text-right" id="cart-tax">{{session('cart')->getTax()}}</td>
                                    <td class="text-right" id="cart-total">{{trans('front.pound')}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>{{trans('front.tax total')}}</strong></td>
                                    <td class="text-right"
                                        id="cart-tax-total">{{session('cart')->getTotalWithTax()}}</td>
                                    <td class="text-right" id="cart-total">{{trans('front.pound')}}</td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>{{trans('front.discount')}}</strong></td>
                                    <td class="text-right" id="cart-tax-total">{{session('cart')->getDiscount()}}</td>
                                    <td class="text-right" id="cart-total">{{trans('front.pound')}}</td>
                                </tr>
                                <tr class="bg-info text-white">
                                    <td class="text-right"><strong>{{trans('front.total to pay')}}</strong></td>
                                    <td class="text-right"
                                        id="cart-tax-total">{{session('cart')->getTotalWithDiscount()}}</td>
                                    <td class="text-right" id="cart-total">{{trans('front.pound')}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="buttons clearfix">
                        <div class=" d-inline-block">
                            <button type="submit" form="order-form"
                                    class="butn butnLight wcolorTxt h6">{{trans('front.order')}}
                            </button>
                        </div>
                    </div>
                </div><!--//main-content-->
            </div>
        </div>

    </section>
    <!-- End Sub Contact-->

@overwrite

@section('script')
    <script>
        $('.quantity-input').on('keyup', function () {
            let input = $(this);
            let val = input.val();
            let price = input.data('price');
            let total = val * price;
            total += ' {{trans('front.pound')}}';

            let id = input.data('id');

            $.get('/cart/' + id + '/' + val + '/update', function (data) {
                input.parent().parent().siblings('.td-price').html(total);
                window.location.reload();
            });
        });

        $(document).on('click', '.btn-remove-cart', function () {
            let id = $(this).data('id');
            $.get('/cart/' + id + '/delete', function (data) {
                window.location.reload();
            });
        });

        $(document).on('click', '.btn-refresh', function () {
            window.location.reload();
        });

        {{--$(document).on('submit', '#coupon-form', function (e) {--}}
        {{--    e.preventDefault();--}}
        {{--    let coupon = $('input[name=coupon]').val();--}}
        {{--    if (coupon.length > 0) {--}}
        {{--        $("#coupon-form").submit();--}}
        {{--    } else {--}}
        {{--        alert('{{trans('front.enter a coupon to apply')}}')--}}
        {{--    }--}}
        {{--});--}}
    </script>
@overwrite