@extends('layouts.front')

@section('content')
<!-- Start Sub Content-->
<section class="sub-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-12 main-sidebar">
                <div class="d-block w-100 wrap">
                    <div class="d-block side-item links">
                        <div class="d-block main-title bordered lcolorTxt h5 f-bold">All Categories</div>
                        <ul class="list-unstyled">
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                        </ul>
                    </div>
                </div>

            </div><!--//main-sidebar-->
            <div class="col-md-9 col-12 main-content">
                <div class="row">
                    <div class="col-12 h5 f-bold main-title bordered lcolorTxt">Page Template Here</div>
                    <div class="col-12 h6">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged. It was
                        popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of
                        Lorem Ipsum.

                    </div>
                    <div class="col-12 h6">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged. It was
                        popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of
                        Lorem IpsumLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.


                    </div>
                </div>
            </div><!--//main-content-->
        </div>
    </div>

</section>
<!-- End Sub Contact-->
@overwrite