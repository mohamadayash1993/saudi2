@extends('layouts.front')

@section('content')
<!-- Start Sub Content-->
<section class="sub-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-12 main-sidebar">
                <div class="d-block w-100 wrap">
                    <div class="d-block side-item links">
                        <div class="d-block main-title bordered lcolorTxt h5 f-bold">My Account</div>
                        <ul class="list-unstyled">
                            <li><a href="#" class="active">Account Information</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                            <li><a href="#">Lorem Ipusm</a></li>
                        </ul>
                    </div>
                </div>

            </div><!--//main-sidebar-->
            <div class="col-md-9 col-12 main-content">
                <div class="row">
                    <div class="col-12 login">
                        <div class="d-block w-100 wcolorBG wrap">
                            <form class="p-0">
                                <div class="h5 f-bold">Newsletter Subscription</div>
                                <div class="h6 f-light">Subscribe</div>
                                <div class="form-group required">
                                    <div class="d-block">
                                        <input type="radio" name="" value="1" class="aa-radio">
                                        &nbsp;Yes
                                        &nbsp;
                                        <input type="radio" name="" value="2" class="aa-radio">
                                        &nbsp;No
                                    </div>
                                </div>
                                <div class="buttons clearfix">
                                    <div class="pull-right">
                                        <button type="button" id="" data-loading-text="Loading..."
                                                class="butn butnLight wcolorTxt h6">Update
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!--//main-content-->
        </div>
    </div>

</section>
<!-- End Sub Contact-->

@overwrite