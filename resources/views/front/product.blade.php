@extends('layouts.front')

@section('content')
    <!-- Start Product Content-->
    <section class="product-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-12">
                    <div id="proGallery" class="d-block w-100"></div>
                    <div class="d-block w-100">
                        <ul class="nav nav-tabs" id="reviewTabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tabreview-1-tab" data-toggle="tab" href="#tabreview-1"
                                   role="tab"
                                   aria-controls="tabreview-1" aria-selected="true">{{trans('front.description')}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tabreview-2-tab" data-toggle="tab" href="#tabreview-2"
                                   role="tab"
                                   aria-controls="profile" aria-selected="false">{{trans('front.reviews')}}
                                    ({{$data["product"]->reviews()->count()}})</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="reviewTabsContent">
                            <div class="tab-pane fade show active" id="tabreview-1" role="tabpanel"
                                 aria-labelledby="tabreview-1-tab">

                                <div class="desc jLeft fcolorTxt h6">
                                    {{getTranslatedAttribute($data["product"] , "body")}}
                                </div>

                            </div>
                            <div class="tab-pane fade" id="tabreview-2" role="tabpanel"
                                 aria-labelledby="tabreview-2-tab">
                                @foreach($data["product"]->reviews()->get() as $review)
                                    <div class="d-block review-item">
                                        <div class="row head">
                                            <div class="col-lg-8 col-6">
                                                <p class="f-bold fcolorTxt">{{$review->name}}</p>
                                            </div>
                                            <div class="col-lg-4 col-6 time">
                                                <span> <i class="fas fa-clock"></i></span>
                                                <span class="fcolorTxt">{{$review->created_at->format('d M Y')}}</span>
                                            </div>
                                        </div>
                                        <div class="row cont">
                                            <div class="col-12 jLeft">
                                                {{$review->body}}
                                            </div>

                                            <div class="col-12 lcolorTxt over-hidden rating">
                                                @for($i = 0 ; $i < $review->rating ; $i++)
                                                    <i class="fas fa-star"></i>
                                                @endfor
                                                @for(;$i < 5 ; $i++)
                                                    <i class="fas fa-star fcolorTxt"></i>
                                                @endfor
                                            </div>
                                        </div>
                                    </div><!--//review-item-->
                                @endforeach

                                <form id="form-review" method="post" action="{{route('front.review')}}">
                                    @csrf
                                    <div class="h5 f-bold">{{trans('front.write review')}}</div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label" for="input-name">
                                                {{trans('front.your name')}}
                                            </label>
                                            <input type="text" name="name" id="input-name"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label"
                                                   for="input-review">{{trans('front.your review')}}</label>
                                            <textarea name="body" rows="5" id="input-review"
                                                      class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="contdrol-label d-block">Rating</label>
                                            {{trans('front.bad')}}&nbsp;
                                            <input type="radio" name="rating" value="1" class="aa-radio">
                                            &nbsp;
                                            <input type="radio" name="rating" value="2" class="aa-radio">
                                            &nbsp;
                                            <input type="radio" name="rating" value="3" class="aa-radio">
                                            &nbsp;
                                            <input type="radio" name="rating" value="4" class="aa-radio">
                                            &nbsp;
                                            <input type="radio" name="rating" value="5" class="aa-radio">
                                            &nbsp; {{trans('front.good')}}&nbsp;
                                        </div>
                                    </div>
                                    <div class="buttons clearfix">
                                        <div class="pull-right">
                                            <button type="button" id="button-review" data-loading-text="Loading..."
                                                    class="butn butnLight wcolorTxt h6">{{trans('front.submit')}}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="prodetails" class="col-lg-6 col-12">
                    <form action="">
                        <div class="d-block h2 f-bold fcolorTxt">{{getTranslatedAttribute($data["product"] , "name")}}</div>
                        <div class="d-block h6 lcolorTxt rating">
                            @for($i = 0 ; $i < $data["product"]->rating ; $i++)
                                <i class="fas fa-star"></i>
                            @endfor
                            @for(; $i < 5 ; $i++)
                                <i class="fas fa-star fcolorTxt"></i>
                            @endfor
                        </div>
                        <div class="d-block details-table">
                            <div class="row">
                                <div class="col-5 f-bold">{{trans('front.brand')}}</div>
                                <div class="col-7">{{getTranslatedAttribute($data["product"]->brand , "name")}}</div>
                            </div>
                            <div class="row">
                                <div class="col-5 f-bold">{{trans('front.code')}}</div>
                                <div class="col-7">{{$data["product"]->id}}</div>
                            </div>
                            <div class="row">
                                <div class="col-5 f-bold">{{trans('front.type')}}</div>
                                <div class="col-7">{{$data["product"]->type}}</div>
                            </div>
                            <div class="row">
                                <div class="col-5 f-bold">{{trans('front.model')}}</div>
                                <div class="col-7">{{$data["product"]->model}}</div>
                            </div>
                        </div>

                        <div class="d-block w-100 price-item">
                            <div class="d-block w-100 over-hidden price">
                                <span class="original h5 f-bold lcolorTxt">{{$data["product"]->price}} {{trans('front.pound')}}</span>
                            </div>
                            <div class="d-block w-100 over-hidden tax">
                                <span class="h6 fcolorTxt">{{trans('front.tax')}}:</span>
                                <span class="h6 f-light fcolorTxt">{{$data["product"]->price * 0.05}} {{trans('front.pound')}}</span>
                            </div>
                        </div>
                        <div class="d-block w-100 options">
                            <div class="d-block filter-item">
                                <span class="h6 fcolorTxt f-bold title">{{trans('front.quantity')}}.</span>
                                <div class="d-block actions">
                                    <span class="mouse butn butnWhite h6 fcolorTxt more">
                                        <i class="fas fa-plus"></i>
                                    </span>
                                    <input name="quantity" type="number" min="1" value="1" class="d-inline-block"/>
                                    <span class="mouse butn butnWhite h6 fcolorTxt less">
                                        <i class="fas fa-minus"></i>
                                    </span>
                                    <a data-id="{{$product->id}}" href="#" class="btn-cart d-inline-block butn butnLight wcolorTxt cap">
                                        <span><i class="fas fa-shopping-cart"></i></span>
                                        <span>{{trans('front.add to cart')}}</span>
                                    </a>
                                    <a data-id="{{$product->id}}" href="#" title="{{trans('front.add to wish')}}" class="fav btn-wish">
                                        <i class="far fa-heart"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>
    <!-- End Product Contact-->


    @include('front.partials.featured')
    @include('front.partials.brands')

@overwrite
@section('script')
    <script>
        $(document).ready(function () {
            //product images url
            let urls = [
                @foreach($data["product"]->images()->get() as $image)
                    '{{asset('storage/'.$image->path)}}',
                @endforeach
            ];
            $('#proGallery').zoomy(urls, {});

        });
    </script>
@overwrite