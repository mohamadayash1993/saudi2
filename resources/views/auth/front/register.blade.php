@extends('layouts.front')

@section('content')

    <!-- Start Sub Content-->
    <section class="sub-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-12 main-sidebar">
                    <div class="d-block w-100 wrap">
                        <div class="d-block side-item links">
                            <div class="d-block main-title bordered lcolorTxt h5 f-bold">{{ trans('front.links') }}</div>
                            <ul class="list-unstyled">
                                <li><a href="{{route('front.about')}}">{{trans('front.about')}}</a></li>
                                <li><a href="{{route('front.contact')}}">{{trans('front.contact')}}</a></li>
                                <li><a href="{{route('front.faq')}}">{{trans('front.faq')}}</a></li>
                            </ul>
                        </div>
                    </div>

                </div><!--//main-sidebar-->
                <div class="col-lg-9 col-md-8 col-12 main-content">
                    <div class="row">
                        <div class="col-md-6 col-12 login">
                            <div class="d-block w-100 wcolorBG wrap">
                                <form class="d-block w-100 p-0" method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="h5 f-bold">{{trans('front.new customer')}}</div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label"
                                                   for="name">{{trans('front.your name')}}</label>
                                            <input type="text" name="name" value="{{old('name')}}" id="name"
                                                   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label"
                                                   for="mobile">{{trans('front.your mobile')}}</label>
                                            <input type="text" name="mobile" value="{{old('mobile')}}" id="email"
                                                   class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('mobile'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label"
                                                   for="email">{{trans('front.your email')}}</label>
                                            <input type="email" name="email" value="{{old('email')}}" id="email"
                                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label"
                                                   for="password">{{trans('front.your password')}}</label>
                                            <input type="password" name="password" value="{{old('password')}}"
                                                   id="password"
                                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="d-block">
                                            <label class="control-label"
                                                   for="password_confirmation">{{trans('front.confirm password')}}</label>
                                            <input type="password" name="password_confirmation" value=""
                                                   id="password_confirmation"
                                                   class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group required">
                                        <div class="d-block">
                                            <button type="submit"
                                                    class="butn butnLight wcolorTxt h6">{{trans('front.register')}}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 login register">

                            <div class="d-block w-100 wcolorBG wrap">
                                <div class="h5 f-bold">{{trans('front.returning customer')}}</div>
                                <div class="h6 f-light jLeft">
                                    {{trans('front.login message')}}
                                </div>
                                <a href="{{route('login')}}"
                                   class="butn butnLight wcolorTxt h6">{{trans('front.login')}}</a>
                            </div>
                        </div>
                    </div>
                </div><!--//main-content-->
            </div>
        </div>

    </section>
    <!-- End Sub Contact-->

@overwrite
