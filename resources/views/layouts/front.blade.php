<!DOCTYPE html>
<html lang="{{getLocale()}}" dir="{{getLocale() == "ar" ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#BB9248"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ isset($title) ? trans('title.'.$title) : trans('front.title')}}</title>

    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css')}}"/>

    @if (getLocale() == "ar")
        <link type="text/css" rel="stylesheet" href="{{ asset('front/css/bootstrap-rtl.min.css') }}"/>
    @endif
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/all.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/animate.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/sal.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/owl.carousel.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/owl.theme.default.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/jQuery.inputSliderRange.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/zoomy.css')}}"/>
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{ asset('front/css/main.css')}}"/>
    @if (getLocale() == "ar")
        <link type="text/css" rel="stylesheet" href="{{ asset('front/css/rtl.css') }}"/>
    @endif
    <link rel="shortcut icon" href="{{ asset('front/imgs/icon.ico')}}"/>

    @stack('style')
    @yield('style')
</head>
<body>

<!-- Start goTop -->
<div id="goTop" class="rounded-circle">
    <a href="#main-header">
        <i class="fas fa-chevron-up"></i>
    </a>
</div>
<!-- End goTop -->
<div class="popup" id="popupimg">
    <div class="closeView">
        <i class="fa fa-times"></i>
    </div>
    <!--//closeView-->
    <div class="content">
    </div>
</div>
<!-- End popup -->
<!-- Start Main Header-->
<section id="main-header" class="waving-bg">
    <div class="top-bar lcolorBG info" id="top-bar">
        <div class="container">
            <div class="row">
                <nav class="aa-topbar row dark">
                    <div class="col-lg-5 col-12">
                        <span class="d-lg-inline-block d-none">{{trans('front.subscribe title')}}</span>
                    </div>
                    <div class="col-lg-7 col-12 aa-menu lcolorBG">
                        <ul class="ml-auto aa-menu-wrap">
                            <li><a href="{{route('front.index')}}" class="aa-link">{{trans('front.home')}}</a></li>
                            <li><a href="{{route('front.about')}}" class="aa-link">{{trans('front.about')}}</a></li>
                            <li><a href="#footer" class="aa-link">{{trans('front.contact')}}</a></li>
                            <li><a href="{{route('front.profile')}}" class="aa-link">{{trans('front.my account')}}</a>
                            </li>
                            <li>
                                @if(getLocale() == "en")
                                    <a rel="alternate" hreflang="ar" class="navbar-nav-link"
                                       href="{{ LaravelLocalization::getLocalizedURL("ar", null, [], true) }}">
                                        <span>العربية</span>
                                        <img src="{{asset('front/imgs/flags/arabic.png')}}" alt="">
                                    </a>
                                @else
                                    <a rel="alternate" hreflang="en" class="navbar-nav-link"
                                       href="{{ LaravelLocalization::getLocalizedURL("en", null, [], true) }}">
                                        <span>English</span>
                                        <img src="{{asset('front/imgs/flags/english.png')}}" alt="">
                                    </a>
                                @endif
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="main-bar wcolorBG">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12">
                    <a href="{{route('front.index')}}">
                        <img src="{{ asset('front/imgs/logo.png')}}" alt="{{trans('front.title')}}" class="logo"
                             style="height:60px"/>
                    </a>
                </div>
                <div class="col-lg-3 col-12 search">
                    <form action="{{route('front.search')}}" method="post">
                        @csrf
                        <input value="{{isset($data["query"]) ? $data["query"] : ''}}" name="query" type="text"
                               class="form-control" placeholder="{{trans('front.search')}}"/>
                        <button type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div>
                <div class="col-lg-6 col-12 aa-menu">
                    <ul class="list-unstyled  ml-auto">
                        <li>
                            <a href="tel:{{$data["phone"]->value}}" class="aa-link">
                                <span><i class="fas fa-phone"></i></span>
                                <span class="d-md-inline-block d-none">{{$data["phone"]->value}}</span>
                            </a>
                        </li>
                        @auth
                            <li>
                                <a href="{{route('front.wishlist')}}" class="aa-link">
                                    <span><i class="fas fa-heart"></i></span>
                                    @php($wishlist = Auth::user()->wishlists()->first())
                                    <span class="badge badge-light">{{isset($wishlist) ? $wishlist->products()->count() : 0}}</span>
                                    <span class="d-md-inline-block d-none">{{ trans('front.wishlist') }}</span>
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="{{route('login')}}" class="aa-link">
                                    <span><i class="fas fa-heart"></i></span>
                                    <span class="badge badge-light">0</span>
                                    <span class="d-md-inline-block d-none">{{ trans('front.wishlist') }}</span>
                                </a>
                            </li>
                        @endauth
                        <li>
                            <a href="{{route('front.cart')}}" class="aa-link">
                                <span><i class="fas fa-shopping-cart"></i></span>
                                <span class="badge badge-light">{{session('cart')->getTotalQuantity()}}</span>
                                <span class="d-md-inline-block d-none">{{ trans('front.cart') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="top-bar links-bar">
        <div class="container grcolorBG">
            <nav class="aa-topbar light row">
                <div class="col-6 aa-link d-lg-none d-inline-block"
                     style="line-height: 50px; font-size: 12pt">{{trans('front.all categories')}}
                </div>
                <a href="#" class="aa-navbutn aa-link d-lg-none d-inline-block"><i class="fas fa-list"></i></a>
                <div class="col-12 aa-menu grcolorBG">
                    <ul class="aa-menu-wrap">
                        @foreach($data["categories"] as $category)
                            <li><a href="{{route('front.category' , $category->id)}}"
                                   class="aa-link">{{getTranslatedAttribute($category , "name")}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</section>
<!-- End Main Header-->

@yield('content')

<!-- Start newsletter-->
<section class="newsletter bg-fx-img" style="background-image: url('{{ asset("front/imgs/newsletter-bg.jpg")}}')">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center wcolorTxt">
                <span class="d-block h2">{{ trans('front.newsletter') }}</span>
            </div>
            <form class="col-lg-6 col-12 offset-lg-3 text-center" action="{{route('front.subscribe')}}" method="post">
                @csrf
                <div class="d-inline-block">
                    <input name="email" type="email" class="form-control white"
                           placeholder="{{trans('front.your email')}}"/>
                    <i class="fas fa-envelope"></i>
                </div>
                <div class="d-inline-block">
                    <button class="butn butnWhite" type="submit">{{trans('front.go')}}!</button>
                </div>
            </form>
            <div class="col-12 text-center follow-links">
                @foreach($data["socials"] as $socail)
                    <a href="{{$socail->link}}"><i class="fab fa-{{$socail->type}}"></i></a>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- End  newsletter-->

<!-- Start footer-->
<footer id="footer" class="grcolorBG fcolorTxt">
    <div class="container pos-rel">
        <div class="row">

            <div class="col-lg-3 col-12">
                <div class="d-block "><img src="{{ asset('front/imgs/logo.png')}}" class="w-100" alt=""/></div>
                <div class="d-block jLeft desc">
                    {{trans('front.footer description')}}
                </div>
            </div>

            <div class="col-lg-3 col-12">
                <div class="d-block "><span class="h5 lcolorTxt">{{ trans('front.account') }}</span></div>
                <ul class="list-unstyled">
                    <li><a href="{{route('front.profile')}}">{{trans('front.my account')}}</a></li>
                    <li><a href="{{route('login')}}">{{trans('front.login')}}</a></li>
                    <li><a href="{{route('front.terms')}}">{{trans('front.terms')}}</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-12">
                <div class="d-block "><span class="h5 lcolorTxt">{{ trans('front.links') }}</span></div>
                <ul class="list-unstyled">
                    <li><a href="{{route('front.about')}}">{{trans('front.about')}}</a></li>
                    <li><a href="{{route('front.contact')}}">{{trans('front.contact')}}</a></li>
                    <li><a href="{{route('front.faq')}}">{{trans('front.faq')}}</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-12">
                <div class="d-block "><span class="h5 lcolorTxt">{{ trans('front.contact') }}</span></div>
                @foreach($data["contacts"] as $contact)
                    <div class="d-block info">
                        <div class="d-inline-block h6"><i class="fas fa-{{getContactIcon($contact)}}"></i></div>
                        <div class="d-inline-block h6 f-light">{{$contact->value}}</div>
                    </div>
                @endforeach
                @foreach($data["addresses"] as $address)
                    <div class="d-block info">
                        <div class="d-inline-block h6"><i class="fas fa-map-marker-alt"></i></div>
                        <div class="d-inline-block h6 f-light">{{getTranslatedAttribute($address , "value")}}</div>
                    </div>
                @endforeach
            </div>

            <div class="text-center col-12">
                <div class="d-inline-block paid-imgs">
                    <img src="{{asset('front/imgs/paypal.png')}}" alt="paypal"/>
                    <img src="{{asset('front/imgs/discover.png')}}" alt="discover"/>
                    <img src="{{asset('front/imgs/maestro.png')}}" alt="maestro"/>
                </div>
                <div class="d-inline-block h6 copyrights">
                    <span>{{ trans('front.designed') }}<i class="fas fa-heart" style="color: red"></i> {{ trans('front.by') }}
                        <a href="https://mntsher.com">{{ trans('front.mntsher') }}</a> -
                    </span>
                    <span>
                        {{ trans('front.all right') }} {{ trans('front.title') }} &copy; <script>{{date('Y')}}</script>
                    </span>
                </div>

            </div>
        </div>
    </div>
</footer>
<!-- End footer-->
<script type="text/javascript" src="{{ asset('front/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/jquery.nicescroll.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/fontawesome.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/sal.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/jQuery.inputSliderRange.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/zoomy.js')}}"></script>
<script type="text/javascript" src="{{ asset('front/js/main.js')}}"></script>

<script>
    $(document).ready(function () {
        $('#price-input').inputSliderRange({
            "min": 10,
            "max": 100000,
            "start": 10
        })
    });

</script>

@stack('script')
@yield('script')
</body>
</html>