<?php


return [
    "title" => "سعودي إعمار",
    "all right" => "جميع الحقوق محفوظة لـ ",
    "designed" => "تم التصميم بكل ",
    "by" => "من قبل ",
    "mntsher" => "منتشر",
    "wishlist" => "قائمة رغباتي",
    "cart" => "سلة الشراء",
    "newsletters" => "سجل الآن للحصول على النشرة الأخبارية",
    "account" => "حسابي",
    "links" => "روابط سريعة",
    "contact" => "اتصل بنا",
    "clients" => "",
    "brands" => "العلامات التجارية",
    "featured" => "",
    "products" => "منتجات مميزة",
    "categories" => "أصناف المنتجات",
    "filters" => "فلتر البحث",
    "subscribe title" => "اشترك لتحصل على 50 % خصم",
    "categoryProducts" => "المنتجات",
    "question" => "لديك استفسار ؟",
    "message" => "راسلنا",
    "send" => "إرسال",
    "subject" => "الموضوع",
    "msg" => "رسالتك ...",
];