/**
 * Saudi Emaar Website
 * Version: 1.0
 * Author: Mntsher company
 * Author URL: https://mntsher.com
 * Designer: Ayat Ahmed
 * Designer URL: instagram.com/ayaat.a7med
 * Copyrights 2019
 */
$(document).ready(function () {
    "use strict";


    function gotop() {
        if ($(window).scrollTop() < 250) {
            $('#goTop').fadeOut();
            if ($(window).width() > 768) {
               // $('#top-bar').removeClass('whitebar');
            } else {

              //  $('#top-bar').addClass('whitebar');
            }
        } else {
            $('#goTop').fadeIn();

          //  $('#top-bar').addClass('whitebar');
        }
    }

   /* function toggleBar() {
        $(".coll-butn").siblings('.navbar-collapse').toggleClass('show');
        $(".coll-butn").find('i').toggleClass('fa-times');
        $('body , .navbar').toggleClass('margBody');
    }*/
    function toggleBar(selector) {
        var targetmenu = selector.parents('.aa-topbar').find('.aa-menu').eq(0);
        if(targetmenu.hasClass('show')){
            $('.aa-menu').removeClass('show');
            $('body , .aa-topbar').removeClass('margBody');
        }else{
            $('.aa-menu').removeClass('show');
            targetmenu.addClass('show');
            $('body , .aa-topbar').addClass('margBody');
        }
        $(".coll-butn").find('i').toggleClass('fa-times');
    }
    function aaToggleShow(e) {
        $(e).toggleClass('show');
    }
    function incrementVal(selector) {
        var item = selector;
        var curVal = item.val();
        if(!(curVal < 1)){
            item.val(parseInt(curVal) + 1 );
        }
    }
    function decrementVal(selector) {
        var item = selector;
        var curVal = item.val();
        if(curVal > 1){
            item.val(parseInt(curVal) - 1 );
        }
    }
    function listPro(select) {
        var item = select;
        item.removeClass('col-xl-3 col-lg-4 col-md-6 col-12').addClass('col-12 trend').find('.wrap').removeClass('d-block text-center').addClass('row').find('.img').addClass('col-lg-3 col-12 p-0 ');
        item.addClass('trend').find('.wrap').find('.content').addClass('col-lg-9 col-12');
    }
    function gridPro(select) {
        var item = select;
        item.addClass('col-xl-3 col-lg-4 col-md-6 col-12').removeClass('col-12 trend').find('.wrap').addClass('d-block text-center').removeClass('row').find('.img').removeClass('col-lg-3 col-12 p-0 ');
        item.removeClass('trend').find('.wrap').find('.content').removeClass('col-lg-9 col-12');
    }
    // pop up setting
    $('.closeView').click(function () {
        $(this).parent('.popup').fadeOut(500);
        $(this).parent('.popup').find("video ,iframe").remove();
        $(this).parent('.popup').find("img").removeAttr('src');
    });
    $(".poImg").on("click", function () {
        var sr = $(this).attr("data-src") , el = "";
        el = "<img  src='" + sr + "' class='d-block m-auto'/>";
        $("#popupimg .content").prepend(el);
        $("#popupimg").fadeIn(500);
    });
    $(".povid").on("click", function () {
        var ssr = $(this).attr("data-src"),isYou = $(this).attr("data-youtube"), ell = "";
        if(isYou == "true"){
            ell = "<iframe src='" + ssr + "' width='100%' height='100%'></iframe>";

        }else{
            ell = "<video autoplay controls><source src='" + ssr + "' type='video/mp4'></video>";
        }
        $("#popupimg .content").prepend(ell);
        $("#popupimg").fadeIn(500);
    });
    $(".popbutn").on("click", function () {
        var sr = $(this).attr("data-popup");
        $(sr).fadeIn(500);
    });
    $(".arrows .prev").click(function () {
        $(this).parents('section').find('.carousel').carousel("prev");
    });
    $('.arrows .next').click(function () {
        $(this).parents('section').find('.carousel').carousel("next");
    });
    $(".owl .prev").click(function () {
        $(this).parents('section').find('.owl-carousel').trigger('prev.owl.carousel');
    });
    $('.owl .next').click(function() {
        $(this).parents('section').find('.owl-carousel').trigger('next.owl.carousel');
    });
    //$('body').niceScroll();

    $('.proj-item').each(function () {
        //alert('11');
        //  $(this).parent().addClass('col-lg-3');
    });
    $('.owl-carousel.products-carousel').owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        autoplay: true,
        responsive: {
            // breakpoint from 0 up
            0: {
                items: 1,
            },
            // breakpoint from 480 up
            767: {
                items: 3,
            },
            // breakpoint from 768 up
            991: {
                items: 5,
                loop: true
            }
        }
    });

    $('.owl-carousel.part').owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        autoplay: true,
        responsive: {
            // breakpoint from 0 up
            0: {
                items: 2,
            },
            // breakpoint from 480 up
            767: {
                items: 3,
            },
            // breakpoint from 768 up
            991: {
                items: 6,
                loop: true
            }
        }
    });

    $('.products-carousel .owl-item').addClass('col-lg-2dot4 col-md-4 col-12 ');
    sal({
        once: false
    });

    $('.setting .grid-btn').on('click' , function () {
        $(this).parents('.products').find('.product-item').each(function () {
            gridPro($(this));
        });
    });

    $('.setting .list-btn').on('click' , function () {
        $(this).parents('.products').find('.product-item').each(function () {
            listPro($(this));
        });
    });
    //filter
    $('.filter .nav-link').on('click', function () {
        var vl = $(this).attr('data-filter');
        $(this).parents('.filter').find('.nav-link').removeClass('active');
        $(this).addClass('active');
        if (vl != 'all') {
            $(this).parents('section').find('.job-item').fadeOut();
            $(this).parents('section').find('.job-item' + '.' + vl).fadeIn();
        } else {
            $(this).parents('section').find('.job-item').fadeIn();
        }
    });

    $(window).on('scroll', function () {
        gotop();
    });
    $(window).on('resize', function () {
        gotop();
    });
    gotop();


    $('.aa-navbutn').on('click', function () {
        if ($(window).width() < 768) {
            toggleBar($(this));
        }
    });
    $('.navbar-collapse a').on('click', function () {
        if ($(window).width() < 768) {
            toggleBar($(this));
        }
    });
    /*
     * Ayat Ahmed Framwork
     */
    $('.aa-dropdown').on('click' , function () {
        aaToggleShow($(this).siblings('.aa-menu-wrap').eq(0));
    });
    $('.aa-dropdown .aa-link:not(.aa-dropdown)').on('click' , function () {
        $(this).parents('.aa-menu').find('.aa-menu-wrap').removeClass('show');
    });
    $('.aa-dropdown').on('mouseover' , function () {
        aaToggleShow($(this).siblings('.aa-menu-wrap').eq(0));
    });
    $('.aa-link:not(.aa-navbutn):not(.aa-dropdown)').on('click' , function () {
        $('.aa-topbar , body').removeClass('margBody');
        $('.aa-menu').removeClass('show');
    });
    $('html').on('click' , function () {
      //  $('.aa-menu ul').removeClass('show');
    });

    function cssStyle() {
        if ($.cookie('sma_style') == 'light') {
            $('link[href="' + site.assets + 'styles/blue.css"]').attr('disabled', 'disabled');
            $('link[href="' + site.assets + 'styles/green.css"]').attr('disabled', 'disabled');
            $('link[href="' + site.assets + 'styles/blue.css"]').remove();
            $('link[href="' + site.assets + 'styles/green.css"]').remove();
            $('<link>')
                .appendTo('head')
                .attr({ type: 'text/css', rel: 'stylesheet' })
                .attr('href', site.assets + 'styles/light.css');
        } else if ($.cookie('sma_style') == 'blue') {
            $('link[href="' + site.assets + 'styles/light.css"]').attr('disabled', 'disabled');
            $('link[href="' + site.assets + 'styles/green.css"]').attr('disabled', 'disabled');
            $('link[href="' + site.assets + 'styles/light.css"]').remove();
            $('link[href="' + site.assets + 'styles/green.css"]').remove();
            $('<link>')
                .appendTo('head')
                .attr({ type: 'text/css', rel: 'stylesheet' })
                .attr('href', '' + site.assets + 'styles/blue.css');
        }  else if ($.cookie('sma_style') == 'green') {
            $('link[href="' + site.assets + 'styles/light.css"]').attr('disabled', 'disabled');
            $('link[href="' + site.assets + 'styles/blue.css"]').attr('disabled', 'disabled');
            $('link[href="' + site.assets + 'styles/light.css"]').remove();
            $('link[href="' + site.assets + 'styles/blue.css"]').remove();
            $('<link>')
                .appendTo('head')
                .attr({ type: 'text/css', rel: 'stylesheet' })
                .attr('href', '' + site.assets + 'styles/green.css');
        } else {
            $('link[href="' + site.assets + 'styles/light.css"]').attr('disabled', 'disabled');
            $('link[href="' + site.assets + 'styles/blue.css"]').attr('disabled', 'disabled');
            $('link[href="' + site.assets + 'styles/green.css"]').attr('disabled', 'disabled');
            $('link[href="' + site.assets + 'styles/light.css"]').remove();
            $('link[href="' + site.assets + 'styles/blue.css"]').remove();
            $('link[href="' + site.assets + 'styles/green.css"]').remove();
        }

        if ($('#sidebar-left').hasClass('minified')) {
            $.cookie('sma_theme_fixed', 'no', { path: '/' });
            $('#content, #sidebar-left, #header').removeAttr('style');
            $('#sidebar-left').removeClass('sidebar-fixed');
            $('#content').removeClass('content-with-fixed');
            $('#fixedText').text('Fixed');
            $('#main-menu-act')
                .addClass('full visible-md visible-lg')
                .show();
            $('#fixed').removeClass('fixed');
        } else {
            if (site.settings.rtl == 1) {
                $.cookie('sma_theme_fixed', 'no', { path: '/' });
            }
            if ($.cookie('sma_theme_fixed') == 'yes') {
                $('#content').addClass('content-with-fixed');
                $('#sidebar-left')
                    .addClass('sidebar-fixed')
                    .css('height', $(window).height() - 80);
                $('#header')
                    .css('position', 'fixed')
                    .css('top', '0')
                    .css('width', '100%');
                $('#fixedText').text('Static');
                $('#main-menu-act')
                    .removeAttr('class')
                    .hide();
                $('#fixed').addClass('fixed');
                $('#sidebar-left').css('overflow', 'hidden');
                $('#sidebar-left').perfectScrollbar({ suppressScrollX: true });
            } else {
                $('#content, #sidebar-left, #header').removeAttr('style');
                $('#sidebar-left').removeClass('sidebar-fixed');
                $('#content').removeClass('content-with-fixed');
                $('#fixedText').text('Fixed');
                $('#main-menu-act')
                    .addClass('full visible-md visible-lg')
                    .show();
                $('#fixed').removeClass('fixed');
                $('#sidebar-left').perfectScrollbar('destroy');
            }
        }
        widthFunctions();
    }




    $(document).ready(function() {
        $('#cssLight').click(function(e) {
            e.preventDefault();
            $.cookie('sma_style', 'light', { path: '/' });
            cssStyle();
            return true;
        });
        $('#cssBlue').click(function(e) {
            e.preventDefault();
            $.cookie('sma_style', 'blue', { path: '/' });
            cssStyle();
            return true;
        });
        $('#cssGreen').click(function(e) {
            e.preventDefault();
            $.cookie('sma_style', 'green', { path: '/' });
            cssStyle();
            return true;
        });
        $('#cssBlack').click(function(e) {
            e.preventDefault();
            $.cookie('sma_style', 'black', { path: '/' });
            cssStyle();
            return true;
        });
        $('#toTop').click(function(e) {
            e.preventDefault();
            $('html, body').animate({ scrollTop: 0 }, 100);
        });
        $('span.more').on('click' , function () {
            incrementVal($(this).parents('.actions').find('input[type=number]'));
        });
        $('span.less').on('click' , function () {
            decrementVal($(this).parents('.actions').find('input[type=number]'));
        });
    });
});
