<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title_ar', 'title_en', 'body_ar', 'body_en', 'image'
    ];

    public function images()
    {
        return $this->belongsToMany(Image::class, 'posts_images', 'post_id', 'image_id');
    }


}
