<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFilter extends Model
{
    protected $table = "products_filters";

    protected $fillable = [
        'sort', 'product_id', 'filter_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function filter()
    {
        return $this->belongsTo(Filter::class, 'filter_id');
    }
}
