<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name_ar', 'name_en', 'body_ar', 'body_en', 'is_top', 'sort', 'parent_id'
    ];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    public function images()
    {
        return $this->belongsToMany(Image::class, 'categories_images', 'category_id', 'image_id');
    }

    public function categoryFilters()
    {
        return $this->hasMany(CategoryFilter::class, 'category_id');
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, 'categories_filters', 'category_id', 'filter_id');
    }
}
