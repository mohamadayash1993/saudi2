<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CategoryFilter extends Model
{
    protected $table = "categories_filters";

    protected $fillable = [
        'sort', 'category_id', 'filter_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function filter()
    {
        return $this->belongsTo(Filter::class, 'filter_id');
    }
}
