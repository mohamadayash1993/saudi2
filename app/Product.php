<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name_ar', 'name_en', 'body_ar', 'body_en', 'model', 'type', 'quantity', 'price', 'sort', 'brand_id', 'category_id'
    ];

    public $orderAttributes = [
        "product_id" => "select",
        "quantity" => "double",
        "sort" => "options",
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function images()
    {
        return $this->belongsToMany(Image::class, 'products_images', 'product_id', 'image_id');
    }

    public function productFilters()
    {
        return $this->hasMany(ProductFilter::class, 'product_id');
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, 'products_filters', 'product_id', 'filter_id');
    }

    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class, 'product_id');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'orders_products', 'product_id', 'order_id');
    }

    public function wishlistProducts()
    {
        return $this->hasMany(WishlistProduct::class, 'product_id');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'product_id');
    }

    public function wishlists()
    {
        return $this->belongsToMany(Product::class, 'wishlists_products', 'product_id', 'wishlist_id');
    }


    public function getRatingAttribute()
    {
        $count = $this->reviews()->count();
        if ($count > 0)
            return $this->reviews()->sum('rating') / $count;
        else
            return 0;
    }
}
