<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'title_ar', 'title_en', 'body_ar', 'body_en', 'image'
    ];


    public function images()
    {
        return $this->belongsToMany(Image::class, 'projects_images', 'project_id', 'image_id');
    }
}
