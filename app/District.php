<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class District extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name_ar', 'name_en', 'active', 'type', 'parent_id'
    ];


    public function parent()
    {
        return $this->belongsTo(District::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(District::class, 'parent_id');
    }
}
