<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests\StoreClient;
use App\Http\Controllers\AdminController as Controller;

class ClientController extends Controller
{
    public function __construct(Client $client)
    {
        $this->model = $client;
        $this->route = 'clients';
        $this->title = 'clients';

        $this->table_attributes = [
            "name" => "text",
        ];

        $this->attributes = [
            "name" => "text",
            "link" => "link",
            "image" => "image",
        ];


        $this->storeRequest = new StoreClient();

        parent::__construct();
    }

}
