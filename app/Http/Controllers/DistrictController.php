<?php

namespace App\Http\Controllers;

use App\District;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\StoreDistrict;
use App\Http\Requests\UpdateDistrict;

class DistrictController extends Controller
{
    public function __construct(District $district)
    {
        $this->model = $district;
        $this->route = 'districts';
        $this->title = 'districts';

        $this->attributes = [
            "name_ar" => "text",
            "name_en" => "text",
            "type" => "options",
            "active" => "boolean"
        ];

        $this->storeRequest = new StoreDistrict();
        $this->updateRequest = new UpdateDistrict();

        $this->actions = ['toggle' => []];

        parent::__construct();
    }
    public function getJson($id){
        $lang=getLocale();
     return $data = District::where('parent_id',$id)->pluck('name_'.$lang,'id')->toArray();

    }
}
