<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\StoreOption;
use App\Http\Requests\UpdateOption;
use App\Option;

class OptionController extends Controller
{
    public function __construct(Option $option)
    {
        $this->model = $option;
        $this->route = 'options';
        $this->title = 'options';

        $this->attributes = [
            "type" => "options",
            "text_ar" => "text",
            "text_en" => "text",
            "code" => "text"
        ];

        $this->storeRequest = new StoreOption();
        $this->updateRequest = new UpdateOption();

        parent::__construct();
    }
}
