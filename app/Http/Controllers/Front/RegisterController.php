<?php

namespace App\Http\Controllers\Front;

use App\Address;
use App\Brand;
use App\Category;
use App\Contact;
use App\Product;
use App\Social;
use App\User;
use App\Http\Controllers\Controller;
use App\UserAddress;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function showRegistrationForm()
    {

        $data["phone"] = Contact::where('type', 'phone')->first();
        $data["categories"] = Category::orderBy('sort')->get();
        $data["contacts"] = Contact::all();
        $data["socials"] = Social::all();
        $data["addresses"] = Address::all();
        $data["brands"] = Brand::orderBy('sort')->get();
        $data["featured"] = Product::orderByDesc('created_at')->limit(10)->get();
        return view('auth.front.register', compact('data'));


    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'string', 'max:14', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'password' => Hash::make($data['password']),
        ]);


        $wishlist = new Wishlist();
        $wishlist->user_id = $user->id;
        $wishlist->save();

        return $user;
    }


}
