<?php

namespace App\Http\Controllers\Front;

use App\Address;
use App\Brand;
use App\Category;
use App\Contact;
use App\Http\Controllers\Controller;
use App\Product;
use App\Social;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $data["phone"] = Contact::where('type', 'phone')->first();
        $data["categories"] = Category::orderBy('sort')->get();
        $data["contacts"] = Contact::all();
        $data["socials"] = Social::all();
        $data["addresses"] = Address::all();
        $data["brands"] = Brand::orderBy('sort')->get();
        $data["featured"] = Product::orderByDesc('created_at')->limit(10)->get();
        return view('auth.front.login' , compact('data'));
    }

    public function authenticated(Request $request, $user)
    {
        if ($user->role == "admin" || $user->role == "entry")
            $this->redirectTo = "/admin/home";
        else
            $this->redirectTo = "/home";
    }
}
