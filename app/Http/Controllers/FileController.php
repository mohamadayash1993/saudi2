<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function single(Request $request)
    {
        if ($request->ajax()) {
            if ($request->hasFile('file_data')) {
                try {

                    $path = $request->file('file_data')->store(
                        $request->model, 'public'
                    );

                    return response()->json(['path' => $path]);
                } catch (\Exception $e) {
                    return response()->json(['error' => trans('app.No file selected')]);
                }
            } else {
                return response()->json(['error' => trans('app.No file selected')]);
            }
        }

        return response();
    }

    public function multiple(Request $request)
    {
        if ($request->ajax()) {
            if ($request->hasFile('file_data')) {
                try {
                    $path = $request->file('file_data')->store(
                        $request->model, 'public'
                    );

                    $attachment = new Image();
                    $attachment->path = $path;
                    $attachment->save();

                    return response()->json(['image_id' => $attachment->id]);
                } catch (\Exception $e) {
                    return response()->json(['error' => trans('app.error message' , ['name' => 'upload'])]);
                }
            } else {
                return response()->json(['error' => trans('app.error message' , ['name' => 'upload'])]);
            }
        }

        return response();
    }
}
