<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\StoreContact;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\UpdateContact;

class ContactController extends Controller
{
    public function __construct(Contact $contact)
    {
        $this->model = $contact;
        $this->route = 'contacts';
        $this->title = 'contacts';

        $this->table_attributes = [
            "type" => "text",
            "value" => "text",
        ];

        $this->attributes = [
            "type" => "options",
            "value" => "text",
        ];

        $this->storeRequest = new StoreContact();
        $this->updateRequest = new UpdateContact();

        parent::__construct();
    }

}
