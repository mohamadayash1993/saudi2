<?php

namespace App\Http\Controllers;


use App\CategoryFilter;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\StoreCategoryFilter;
use App\Http\Requests\StoreProductFilter;
use App\Http\Requests\UpdateCategoryFilter;
use App\Http\Requests\UpdateProductFilter;
use App\ProductFilter;

class ProductFilterController extends Controller
{
    public function __construct(ProductFilter $productFilter)
    {
        $this->model = $productFilter;
        $this->route = 'productFilters';
        $this->title = 'productFilters';

        $this->attributes = [
            "filter_id" => "select",
            "product_id" => "select",
            "sort" => "options",
        ];

        $this->storeRequest = new StoreProductFilter();
        $this->updateRequest = new UpdateProductFilter();

        parent::__construct();
    }

}
