<?php

namespace App\Http\Controllers;

use App\Form;
use App\Http\Controllers\AdminController as Controller;

class FormController extends Controller
{
    public function __construct(Form $form)
    {
        $this->model = $form;
        $this->route = 'forms';
        $this->title = 'forms';

        $this->table_attributes = [
            "name" => "text",
            "email" => "text",
            "mobile" => "text",
        ];

        $this->attributes = [
            "name" => "text",
            "email" => "text",
            "mobile" => "text",
            "body" => "text",
        ];


        parent::__construct();
    }
}
