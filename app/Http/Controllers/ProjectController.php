<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePost;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\UpdatePost;
use App\Project;

class ProjectController extends Controller
{
    public function __construct(Project $project)
    {
        $this->model = $project;
        $this->route = 'projects';
        $this->title = 'projects';

        $this->table_attributes = [
            'title' => 'text',
        ];


        $this->attributes = [
            'title_ar' => 'text',
            'title_en' => 'text',
            'body_ar' => 'textarea',
            'body_en' => 'textarea',
            'image' => 'image',
            'images' => 'images',
        ];

        $this->storeRequest = new StorePost();
        $this->updateRequest = new UpdatePost();

        parent::__construct();
    }

}
