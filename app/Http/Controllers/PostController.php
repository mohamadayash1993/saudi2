<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePost;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\UpdatePost;
use App\Post;

class PostController extends Controller
{
    public function __construct(Post $post)
    {
        $this->model = $post;
        $this->route = 'posts';
        $this->title = 'posts';

        $this->table_attributes = [
            'title' => 'text',
        ];


        $this->attributes = [
            'title_ar' => 'text',
            'title_en' => 'text',
            'body_ar' => 'textarea',
            'body_en' => 'textarea',
            'image' => 'image',
            "images" => "images"
        ];


        $this->storeRequest = new StorePost();
        $this->updateRequest = new UpdatePost();

        parent::__construct();
    }

}
