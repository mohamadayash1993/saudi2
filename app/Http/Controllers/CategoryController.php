<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreCategory;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\UpdateCategory;
use App\Category;

class CategoryController extends Controller
{
    public function __construct(Category $category)
    {
        $this->model = $category;
        $this->route = 'categories';
        $this->title = 'categories';

        $this->table_attributes = [
            "name" => "text",
            "products" => "text",
        ];

        $this->attributes = [
            "name_ar" => "text",
            "name_en" => "text",
            "parent_id" => "select",
            "body_ar" => "text",
            "body_en" => "text",
            "is_top" => "options",
            "sort" => "options",
        ];


        $this->storeRequest = new StoreCategory();
        $this->updateRequest = new UpdateCategory();

        parent::__construct();
    }

}
