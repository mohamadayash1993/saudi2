<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreSocial;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\UpdateSocial;
use App\Social;

class SocialController extends Controller
{
    public function __construct(Social $social)
    {
        $this->model = $social;
        $this->route = 'socials';
        $this->title = 'socials';

        $this->table_attributes = [
            "type" => "text",
        ];

        $this->attributes = [
            "type" => "options",
            "link" => "text",
        ];

        $this->storeRequest = new StoreSocial();
        $this->updateRequest = new UpdateSocial();

        parent::__construct();
    }

}
