<?php

namespace App\Http\Controllers;


use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\StoreProduct;
use App\Http\Requests\UpdateProduct;
use App\Product;

class ProductController extends Controller
{
    public function __construct(Product $product)
    {
        $this->model = $product;
        $this->route = 'products';
        $this->title = 'products';

        $this->table_attributes = [
            'name' => 'text',
            'category' => 'text',
            'brand' => 'text',
        ];


        $this->attributes = [
            'name_ar' => 'text',
            'name_en' => 'text',
            'body_ar' => 'textarea',
            'body_en' => 'textarea',
            'model' => 'text',
            'type' => 'text',
            'quantity' => 'text',
            'price' => 'text',
            'sort' => 'options',
            'category_id' => 'select',
            'brand_id' => 'select',
            'images' => 'images',
        ];


        $this->storeRequest = new StoreProduct();
        $this->updateRequest = new UpdateProduct();

        parent::__construct();
    }

}
