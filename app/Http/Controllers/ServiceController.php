<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreService;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\UpdateService;
use App\Service;

class ServiceController extends Controller
{
    public function __construct(Service $service)
    {
        $this->model = $service;
        $this->route = 'services';
        $this->title = 'services';

        $this->table_attributes = [
            'title' => 'text',
        ];


        $this->attributes = [
            'title_ar' => 'text',
            'title_en' => 'text',
            'body_ar' => 'textarea',
            'body_en' => 'textarea',
            'image' => 'image',
        ];

        $this->storeRequest = new StoreService();
        $this->updateRequest = new UpdateService();

        parent::__construct();
    }

}
