<?php

namespace App\Http\Controllers;

use App\Address;
use App\Brand;
use App\Contact;
use App\Coupon;
use App\Order;
use App\OrderProduct;
use App\Product;
use App\Category;
use App\Slide;
use App\Social;
use App\UserAddress;
use App\WishlistProduct;
use Illuminate\Support\Facades\Auth;


class FrontController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['profile', 'checkout']);
    }

    private function getLayoutData()
    {
        $data["phone"] = Contact::where('type', 'phone')->first();
        $data["categories"] = Category::orderBy('sort')->get();
        $data["contacts"] = Contact::all();
        $data["socials"] = Social::all();
        $data["addresses"] = Address::all();
        $data["brands"] = Brand::orderBy('sort')->get();
        $data["featured"] = Product::orderByDesc('created_at')->limit(10)->get();

        return $data;
    }

    public function addAddress()
    {
        $this->validate(\request(), [
            'name' => 'required',
            'details' => 'required',
        ]);


        $address = new UserAddress();
        $address->user_id = \request()->user()->id;
        $address->name = \request('name');
        $address->details = \request('details');
        $address->save();

        if (request()->has('checkout') && request('checkout') == 1)
            return redirect()->route('front.checkout');


        return redirect()->route('front.profile');
    }

    public function deleteAddress($id)
    {
        $address = UserAddress::find($id);
        $address->delete();
    }

    public function removeProductFromWishlist($id)
    {
        $product = WishlistProduct::where('wishlist_id', Auth::user()->wishlists->id)->where('product_id', $id)->first();
        if ($product)
            $product->delete();

    }

    public function removeProductFromCart($id)
    {
        $cart = session('cart');
        if ($cart) {
            $cart->removeFromCart($id);
            session()->forget('cart');
            session()->put('cart', $cart);
        }
    }


    public function addProductToCart($id)
    {
        $cart = session('cart');

        if ($cart) {
            session()->forget('cart');
        } else {
            $cart = new \Cart();
        }

        $cart->addToCart($id);

        session()->put('cart', $cart);
    }

    public function updateQuantity($id, $quantity)
    {
        $cart = session('cart');

        if ($cart) {
            session()->forget('cart');
        } else {
            $cart = new \Cart();
        }

        $cart->updateQuantity($id, $quantity);

        session()->put('cart', $cart);
    }

    public function addProductToWishlist($id)
    {
        $product = WishlistProduct::where('wishlist_id', Auth::user()->wishlists->id)->where('product_id', $id)->first();
        if (is_null($product)) {
            $product = new WishlistProduct();
            $product->wishlist_id = Auth::user()->wishlists->id;
            $product->product_id = $id;
            $product->save();
        }
    }

    public function apply()
    {
        $coupon = request('coupon');

        $couponObj = Coupon::where('code', $coupon)->first();
        if ($couponObj) {
            $cart = session()->get('cart');
            if ($cart) {
                $cart->setCoupon($couponObj);
                session()->forget('cart');
                session()->put('cart', $cart);
            }
        } else {
            session()->put("message", trans('front.not found'));
        }

        return redirect()->route('front.cart');
    }

    public function wishlist()
    {
        $data = $this->getLayoutData();
        return view('front.wishlist', compact('data'));
    }

    public function cart()
    {
        $data = $this->getLayoutData();
        $cart = session()->get('cart');
        if (is_null($cart)) {
            $cart = new \Cart();
            session()->put('cart', $cart);
        }

        return view('front.cart', compact('data'));
    }

    public function home()
    {
        $data = $this->getLayoutData();

        $data["slides"] = Slide::orderBy('order')->get();

        if (!session()->has('cart')) {
            $cart = new \Cart();
            session()->put('cart', $cart);
        }
        return view('front.index', compact('data'));
    }

    public function checkout()
    {
        $data = $this->getLayoutData();

        return view('front.checkout', compact('data'));
    }

    public function execute()
    {
        $data = $this->getLayoutData();
        $data["billing_address"] = UserAddress::find(request('billing_address'));
        $data["delivery_address"] = UserAddress::find(request('delivery_address'));

        return view('front.confirm', compact('data'));
    }

    public function order()
    {
        $cart = session('cart');
        foreach ($cart->getProducts() as $product) {
            if ($product->cart_quantity > $product->quantity) {
                session()->put('order_message', trans('front.out of stock'));
                return redirect()->back();
            }
        }

        $order = new Order();
        $order->user_id = request()->user()->id;
        $order->status = "pending";
        $order->total_price = $cart->getTotalWithDiscount();
      //  $order->delivery_address = request('delivery_address');
       // $order->billing_address = request('billing_address');
        $order->save();

        foreach ($cart->getProducts() as $product) {
            $order_product = new OrderProduct();
            $order_product->order_id = $order->id;
            $order_product->product_id = $product->id;
            $order_product->quantity = $product->cart_quantity;
            $order_product->save();

            $p = Product::find($product->id);
            $p->quantity = $p->quantity - $product->cart_quantity;
            $p->save();
        }

        session()->put('message', trans('front.ordered successfully'));
        return redirect()->route('front.profile');

    }

    public function profile()
    {
        $data = $this->getLayoutData();

        return view('front.account', compact('data'));
    }


    public function category($id)
    {
        $data = $this->getLayoutData();

        $data["category"] = Category::find($id);

        return view('front.category', compact('data'));
    }


    public function product($id)
    {
        $data = $this->getLayoutData();

        $data["product"] = Product::find($id);

        return view('front.product', compact('data'));
    }

    public function search()
    {
        $data = $this->getLayoutData();

        $query = Product::query();

        if (request()->has('query')) {
            $query->where('name_ar', 'like', '%' . request('query') . '%')
                ->orWhere('name_en', 'like', '%' . request('query') . '%')
                ->orWhere('body_ar', 'like', '%' . request('query') . '%')
                ->orWhere('body_en', 'like', '%' . request('query') . '%')
                ->orWhere('model', 'like', '%' . request('query') . '%')
                ->orWhere('type', 'like', '%' . request('query') . '%')
                ->orWhereHas('brand', function ($query) {
                    return $query->where('name_ar', 'like', '%' . request('query') . '%')
                        ->orWhere('name_en', 'like', '%' . request('query') . '%');
                })->orWhereHas('category', function ($query) {
                    return $query->where('name_ar', 'like', '%' . request('query') . '%')
                        ->orWhere('name_en', 'like', '%' . request('query') . '%');
                });
        }
        $data["products"] = $query->paginate(10);
        $data["query"] = request('query');

        return view('front.search', compact('data'));
    }

    public function contact(){
        return view('front.contact');
    }

    public function sendContactMsesage(Request $request){
        //Send Email Here ...
    }
}
