<?php

namespace App\Http\Controllers;

use App\Address;
use App\Coupon;
use App\Http\Requests\StoreAddress;
use App\Http\Requests\StoreCoupon;
use App\Http\Requests\UpdateAddress;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\UpdateCoupon;

class CouponController extends Controller
{
    public function __construct(Coupon $coupon)
    {
        $this->model = $coupon;
        $this->route = 'coupons';
        $this->title = 'coupons';

        $this->table_attributes = [
            "code" => "text",
            "percentage" => "text",
            "active" => "text",
            "usage" => "text",
        ];

        $this->attributes = [
            "code" => "text",
            "percentage" => "text",
            "active" => "options",
        ];


        $this->storeRequest = new StoreCoupon();
        $this->updateRequest = new UpdateCoupon();

        parent::__construct();
    }

}
