<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Filter;
use App\Http\Requests\StoreContact;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\StoreFilter;
use App\Http\Requests\UpdateContact;
use App\Http\Requests\UpdateFilter;

class FilterController extends Controller
{
    public function __construct(Filter $filter)
    {
        $this->model = $filter;
        $this->route = 'filters';
        $this->title = 'filters';

        $this->table_attributes = [
            "name" => "text",
        ];

        $this->attributes = [
            "name_ar" => "text",
            "name_en" => "text",
            "type" => "options",
            "options" => "options",
        ];

        $this->storeRequest = new StoreFilter();
        $this->updateRequest = new UpdateFilter();

        parent::__construct();
    }

}
