<?php

namespace App\Http\Controllers;


use App\CategoryFilter;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\StoreCategoryFilter;
use App\Http\Requests\UpdateCategoryFilter;

class CategoryFilterController extends Controller
{
    public function __construct(CategoryFilter $categoryFilter)
    {
        $this->model = $categoryFilter;
        $this->route = 'categoryFilters';
        $this->title = 'categoryFilters';

        $this->attributes = [
            "filter_id" => "select",
            "category_id" => "select",
            "sort" => "options",
        ];

        $this->storeRequest = new StoreCategoryFilter();
        $this->updateRequest = new UpdateCategoryFilter();

        parent::__construct();
    }

}
