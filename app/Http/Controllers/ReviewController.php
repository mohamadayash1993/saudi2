<?php

namespace App\Http\Controllers;

use App\Form;
use App\Http\Controllers\AdminController as Controller;
use App\Review;

class ReviewController extends Controller
{
    public function __construct(Review $review)
    {
        $this->model = $review;
        $this->route = 'reviews';
        $this->title = 'reviews';


        $this->attributes = [
            "name" => "text",
            "body" => "text",
            "rating" => "number",
            "product" => "select",
        ];

        parent::__construct();
    }
}
