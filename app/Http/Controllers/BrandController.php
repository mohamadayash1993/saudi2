<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Requests\StoreBrand;
use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\UpdateBrand;

class BrandController extends Controller
{
    public function __construct(Brand $brand)
    {
        $this->model = $brand;
        $this->route = 'brands';
        $this->title = 'brands';

        $this->table_attributes = [
            "name" => "text",
            "sort" => "text",
        ];

        $this->attributes = [
            "name_ar" => "text",
            "name_en" => "text",
            "image" => "image",
            "sort" => "options",
        ];


        $this->storeRequest = new StoreBrand();
        $this->updateRequest = new UpdateBrand();

        parent::__construct();
    }

}
