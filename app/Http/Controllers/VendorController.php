<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVendor;
use App\Http\Requests\UpdateVendor;
use App\Http\Controllers\AdminController as Controller;
use App\Vendor;

class VendorController extends Controller
{
    public function __construct(Vendor $vendor)
    {
        $this->model = $vendor;
        $this->route = 'vendors';
        $this->title = 'vendors';

        $this->table_attributes = [
            "name" => "text",
        ];

        $this->attributes = [
            "name" => "text",
            "link" => "link",
            "image" => "image",
        ];


        $this->storeRequest = new StoreVendor();
        $this->updateRequest = new UpdateVendor();

        parent::__construct();
    }

}
