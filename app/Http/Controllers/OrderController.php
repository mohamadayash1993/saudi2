<?php

namespace App\Http\Controllers;


use App\Http\Controllers\AdminController as Controller;
use App\Http\Requests\StoreOrder;
use App\Http\Requests\UpdateOrder;
use App\Order;

class OrderController extends Controller
{
    public function __construct(Order $order)
    {
        $this->model = $order;
        $this->route = 'orders';
        $this->title = 'orders';

        $this->table_attributes = [
            "total_price" => "text",
            "user_id" => "text",
            "status" => "text",
        ];

        $this->attributes = [
            "total_price" => "number",
            "user_id" => "select",
            "status" => "text",
        ];


        $this->relations = [
            "products" => "has",
        ];

        $this->pivot = [
            "quantity" => "products",
        ];

        $this->storeRequest = new StoreOrder();
        $this->updateRequest = new UpdateOrder();

        parent::__construct();
    }

}
