<?php

namespace App\Http\Controllers;

use App\Address;
use App\Http\Requests\StoreAddress;
use App\Http\Requests\UpdateAddress;
use App\Http\Controllers\AdminController as Controller;

class AddressController extends Controller
{
    public function __construct(Address $address)
    {
        $this->model = $address;
        $this->route = 'addresses';
        $this->title = 'addresses';

        $this->table_attributes = [
            "value" => "text",
        ];

        $this->attributes = [
            "value_ar" => "text",
            "value_en" => "text",
            "location" => "location",
        ];


        $this->storeRequest = new StoreAddress();
        $this->updateRequest = new UpdateAddress();

        parent::__construct();
    }

}
