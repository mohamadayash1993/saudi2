<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreOrderProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    /*public function authorize()
    {
        return in_array(Auth::user()->role, ["customer"]);
    }
*/
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'required|numeric',

        ];
    }

    public function messages()
    {
        return [
            'quantity.required' => trans('validation.required'),
            'quantity.numeric' => trans('validation.numeric'),

        ];
    }
}
