<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return in_array(Auth::user()->role, ["admin", "entry"]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_ar' => 'required',
            'title_en' => 'required',
            'body_ar' => 'required',
            'body_en' => 'required',
            'image' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title_en.required' => trans('validation.required'),
            'title_ar.required' => trans('validation.required'),
            'body_ar.required' => trans('validation.required'),
            'body_en.required' => trans('validation.required'),
            'image.required' => trans('validation.required'),
        ];
    }
}
