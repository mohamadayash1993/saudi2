<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreDistrict extends FormRequest
{
    public function authorize()
    {
        return in_array(Auth::user()->role, ["admin", "entry"]);
    }

    public function rules()
    {
        return [
            "name_ar" => "required",
            "name_en" => "required",
            "type" => "required",
            "active" => "required",
        ];
    }

    public function messages()
    {
        return [
            "name_ar.required" => trans('validation.required'),
            "name_en.required" => trans('validation.required'),
            "active.required" => trans('validation.required'),
            "type.required" => trans('validation.required'),
        ];
    }
}
