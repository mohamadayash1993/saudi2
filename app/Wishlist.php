<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $fillable = [
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function wishlistProducts()
    {
        return $this->hasMany(WishlistProduct::class, 'wishlist_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'wishlists_products', 'wishlist_id', 'product_id');
    }
}
