<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $table = "users_addresses";
    protected $fillable = [
        "name", "details", "user_id"
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
