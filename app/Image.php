<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'path'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_images', 'image_id', 'product_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'categories_images', 'image_id', 'category_id');
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'posts_images', 'image_id', 'post_id');
    }

    public function projects()
    {
        return $this->belongsToMany(Post::class, 'projects_images', 'image_id', 'project_id');
    }
}
