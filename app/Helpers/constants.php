<?php

define('MODELS',
    [
        'address', 'brand', 'category', 'client', 'contact', 'district', 'filter',
        'form', 'option', 'order', 'post', 'product', 'project', 'service', 'slide', 'coupon',
        'social', 'user', 'vendor', 'wishlist', 'categoryFilter', 'productFilter', 'review'
    ]
);