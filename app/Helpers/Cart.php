<?php

class  Cart
{
    protected $products;
    protected $coupon;

    public function __construct()
    {
        $this->products = new \Illuminate\Database\Eloquent\Collection();
    }

    public function addToCart($id)
    {
        $found = false;
        foreach ($this->products as $product) {
            if ($product->id == $id)
                $found = true;
        }

        if (!$found) {
            $product = \App\Product::find($id);
            $product->cart_quantity = 1;
            $this->products->add($product);
        }

    }

    public function updateQuantity($id, $quantity)
    {
        $products = $this->products;
        for ($i = 0; $i < $products->count(); $i++) {
            if ($products[$i]->id == $id) {
                $products[$i]->cart_quantity = $quantity;
            }
        }
        $this->products = $products;
    }

    public function getTotal()
    {
        $total = 0;
        foreach ($this->products as $product) {
            $total += $product->cart_quantity * $product->price;
        }

        return $total;
    }

    public function getTax()
    {
        return $this->getTotal() * 0.05;
    }

    public function getTotalWithTax()
    {
        return $this->getTotal() + $this->getTax();
    }

    public function getDiscount()
    {
        if (isset($this->coupon)) {
            return $this->getTotalWithTax() * ($this->coupon->percentage / 100);
        }

        return 0;
    }


    public function getTotalWithDiscount()
    {
        return $this->getTotalWithTax() - $this->getDiscount();
    }

    public function removeFromCart($id)
    {
        $products = new \Illuminate\Database\Eloquent\Collection();
        foreach ($this->products as $product) {
            if ($product->id != $id)
                $products->add($product);
        }

        $this->products = $products;
    }

    public function getTotalQuantity()
    {
        return $this->products->count();
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;
    }

    public function getCoupon()
    {
        return $this->coupon;
    }
}