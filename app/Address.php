<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'value_ar', 'value_en', 'longitude', 'latitude'
    ];

}
