<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//can be removed and replaced with withPivot in many relation orders <=> products
class OrderProduct extends Model
{

    protected $table = "orders_products" ;

    protected $fillable = [
        'quantity', 'sort', 'order_id', 'product_id'
    ];


    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

}
