<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = [
        'name_ar', 'name_en', 'image', 'sort',
    ];

    public function products(){
        return $this->hasMany( Product::class, 'brand_id');
    }
}
