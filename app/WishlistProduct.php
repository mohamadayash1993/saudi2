<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//can be removed and replaced with withPivot in many relation wishlists <=> products
class WishlistProduct extends Model
{
    protected $table = "wishlists_products";

    protected $fillable = [
        'sort', 'wishlist_id', 'product_id'
    ];

    public function wishlist()
    {
        return $this->belongsTo(Wishlist::class, 'wishlist_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

}
