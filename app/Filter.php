<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $fillable = [
        'name_ar', 'name_en' , 'options' , 'type'
    ];

    public function categoryFilters()
    {
        return $this->hasMany(CategoryFilter::class, 'filter_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Filter::class, 'categories_filters', 'filter_id', 'category_id');
    }

    public function productFilters()
    {
        return $this->hasMany(ProductFilter::class, 'filter_id');
    }


    public function products()
    {
        return $this->belongsToMany(Filter::class, 'products_filters', 'filter_id', 'product_id');
    }
}
